{- |
   Module : App.Projects
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module App.Projects ( at
                    , spaces
                    , projects
                    ) where

import Prelude ( ($)
               , (.)
               , (!!)
               , (<$>)
               , Eq ()
               , Int ()
               , Enum ()
               , Show ()
               , Maybe ( Just
                       , Nothing
                       )
               , String ()
               , Bounded ()
               , show
               , pred
               , minBound
               , enumFrom
               )

import Data.Char (toLower)

import System.FilePath ((</>))

import XMonad.Actions.SpawnOn (spawnHere)
import XMonad.Actions.DynamicProjects ( Project (Project)
                                      , projectName
                                      , projectStartHook
                                      , projectDirectory
                                      )

import App.Alias ( (+++)
                 , (<..>)
                 , term
                 , files
                 , binDir
                 , inTerm
                 , browser
                 , homeDir
                 , configDir
                 , dataDrive
                 , andExecute
                 )

data Workspaces = GEN
                | NET
                | AVI
                | GHC
                | CHT
                | MSC
                | MON
                | TMP
                | PRO
                | GME deriving (Eq, Show, Enum, Bounded)

spaces :: [String]
spaces = (toLower <..>) $ show <$> (enumFrom minBound :: [Workspaces])

at :: Int -> String
at = (spaces !!) . pred

projects :: [Project]
projects = [ Project { projectName = at 1
                     , projectDirectory = homeDir
                     , projectStartHook = Just $ do spawnHere term
                                                    spawnHere term
                     }
           , Project { projectName = at 2
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere $ browser
                     }
           , Project { projectName = at 3
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere . inTerm . andExecute $ files +++ dataDrive </> "Movies"
                     }
           , Project { projectName = at 4
                     , projectDirectory = configDir
                     , projectStartHook = Just $ do spawnHere term
                                                    spawnHere term
                     }
           , Project { projectName = at 5
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at 6
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at 7
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at 8
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at 9
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at 10
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere $ binDir </> "mc"
                     }
           ]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
