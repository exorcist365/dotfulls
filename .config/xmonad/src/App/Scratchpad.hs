{- |
   Module : App.Scratchpad
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module App.Scratchpad ( fmSP
                      , ghciSP
                      , mailSP
                      , ocamlSP
                      , musicSP
                      , terminalSP
                      , scratchpads
                      ) where

import Prelude ( ($)
               , (/)
               , Bool ()
               , String ()
               )

import Data.List (unwords)

import System.FilePath ((</>))

import XMonad ( (=?)
              , Query ()
              , ManageHook ()
              , className
              )
import XMonad.StackSet (RationalRect (RationalRect))

import XMonad.Util.NamedScratchpad ( NamedScratchpad ( NS
                                                     , cmd
                                                     , hook
                                                     , name
                                                     , query
                                                     )
                                   , customFloating
                                   )

import App.Alias ( term
                 , mail
                 , files
                 , music
                 , configHome
                 )

fmSP, ghciSP, mailSP, ocamlSP, musicSP, terminalSP :: String
fmSP = "FFF"
ghciSP = "GHCI"
mailSP = "Mail"
ocamlSP = "OCaml"
musicSP = "Music"
terminalSP = "Terminal"

scratchpads :: [NamedScratchpad]
scratchpads = [ NS { name = ghciSP
                   , query = isGHCI
                   , cmd = spawnGHCI
                   , hook = centerFloat
                   }
              , NS { name = ocamlSP
                   , query = isOcaml
                   , cmd = spawnOcaml
                   , hook = centerFloat
                   }
              , NS { name = musicSP
                   , query = isMusic
                   , cmd = spawnMusic
                   , hook = centerFloat
                   }
              , NS { hook = topFloat
                   , name = terminalSP
                   , query = isTerminal
                   , cmd = spawnTerminal
                   }
              , NS { name = fmSP
                   , query = isFM
                   , cmd = spawnFM
                   , hook = centerFloat
                   }
              , NS { name = mailSP
                   , query = isMail
                   , cmd = spawnMail
                   , hook = centerFloat
                   }
              ] where
                  spawnFM, spawnGHCI, spawnMail, spawnOcaml, spawnMusic, spawnTerminal :: String
                  spawnFM = unwords [term, "-c", fmSP, "-e", files]
                  spawnGHCI = unwords [term, "-c", ghciSP, "-e", "stack exec -- ghci", "-v0", "-ghci-script", configHome </> "ghci/ghci.conf"]
                  spawnMail = unwords [term, "-c", mailSP, "-e", mail]
                  spawnOcaml = unwords [term, "-c", ocamlSP, "-e", "myutop"]
                  spawnMusic = unwords [term, "-c", musicSP, "-e", music]
                  spawnTerminal = unwords [term, "-c", terminalSP]

                  isFM, isGHCI, isMail, isOcaml, isMusic, isTerminal :: Query Bool
                  isFM = className =? fmSP
                  isGHCI = className =? ghciSP
                  isMail = className =? mailSP
                  isOcaml = className =? ocamlSP
                  isMusic = className =? musicSP
                  isTerminal = className =? terminalSP

                  topFloat, centerFloat :: ManageHook
                  topFloat = customFloating $ RationalRect 0 0 1 0.45
                  centerFloat = customFloating $ RationalRect (1/6) (1/6) (2/3) (2/3)

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
