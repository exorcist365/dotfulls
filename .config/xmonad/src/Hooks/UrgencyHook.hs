{- |
   Module : Hooks.UrgencyHook
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.UrgencyHook (LibUrgencyHook (LibUrgencyHook)) where

import Prelude ( (<$>)
               , Read ()
               , Show (show)
               , Maybe (Just)
               )

import XMonad ( gets
              , windowset
              )
import XMonad.StackSet (findTag)

import XMonad.Hooks.UrgencyHook ( UrgencyHook ()
                                , urgencyHook
                                )

import XMonad.Util.NamedWindows (getName)

import App.Alias ( (+++)
                 , spawnList
                 )

data LibUrgencyHook = LibUrgencyHook deriving (Read, Show)

instance UrgencyHook LibUrgencyHook where
    urgencyHook LibUrgencyHook w = do
        name <- getName w
        (Just idx) <- findTag w <$> gets windowset

        spawnList ["notify-send", "-u", "low", "workspace:" +++ idx, show name]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
