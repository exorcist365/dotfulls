{- |
   Module : Hooks.StartupHook
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.StartupHook (starter) where

import Prelude (mconcat)

import XMonad ( X ()
              , def
              , startupHook
              )

import XMonad.Hooks.SetWMName (setWMName)

starter :: X ()
starter = mconcat [ startupHook def
                  , setWMName "LG3D"
                  ]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
