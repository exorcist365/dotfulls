{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{- |
   Module : Hooks.LayoutHook
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.LayoutHook ( ToggleHints (ToggleHints)
                        , ToggleCentered (ToggleCentered)
                        , layout
                        , emConfig
                        , fullscreenCustom
                        ) where

import Prelude ( ($)
               , (.)
               , (+)
               , (/)
               , Eq ()
               , Int ()
               , Num ()
               , Read ()
               , Show ()
               , Bool ( True
                      , False
                      )
               , Double ()
               , String ()
               , Integer ()
               , zip
               , sqrt
               , const
               , repeat
               , maxBound
               , toRational
               )

import Data.Word (Word64 ())

import Graphics.X11.Types ( xK_a
                          , xK_c
                          , xK_s
                          , xK_d
                          , xK_f
                          , xK_h
                          , xK_j
                          , xK_k
                          , xK_l
                          , xK_Escape
                          )

import XMonad ( (|||)
              , Full (Full)
              , Window ()
              , XConfig (layoutHook)
              , LayoutClass ()
              )

import XMonad.Actions.EasyMotion ( ChordKeys (AnyKeys)
                                 , EasyMotionConfig ( bgCol
                                                    , sKeys
                                                    , txtCol
                                                    , emFont
                                                    , overlayF
                                                    , borderPx
                                                    , cancelKey
                                                    , borderCol
                                                    , maxChordLen
                                                    )
                                 , fixedSize
                                 )

import XMonad.Hooks.ManageDocks (avoidStruts)

import XMonad.Layout.Gaps ( Gaps ()
                          , Direction2D ( U
                                        , D
                                        , R
                                        , L
                                        )
                          , gaps
                          )
import XMonad.Layout.Tabbed ( Theme ( decoWidth
                                    , decoHeight
                                    , activeColor
                                    , urgentColor
                                    , inactiveColor
                                    , activeTextColor
                                    , urgentTextColor
                                    , inactiveTextColor
                                    , activeBorderColor
                                    , activeBorderWidth
                                    , urgentBorderColor
                                    , urgentBorderWidth
                                    , inactiveBorderColor
                                    , inactiveBorderWidth
                                    )
                            , Shrinker (shrinkIt)
                            , def
                            , addTabsBottom
                            )
import XMonad.Layout.Hidden (hiddenWindows)
import XMonad.Layout.Spacing ( Border (Border)
                             , Spacing ()
                             , spacingRaw
                             )
import XMonad.Layout.Reflect (REFLECTX (REFLECTX))
import XMonad.Layout.Renamed ( Rename ( Replace
                                      , CutWordsLeft
                                      )
                             , renamed
                             )
import XMonad.Layout.Simplest (Simplest (Simplest))
import XMonad.Layout.NoBorders ( Ambiguity (Never)
                               , ConfigurableBorder ()
                               , noBorders
                               , lessBorders
                               )
import XMonad.Layout.SubLayouts (subLayout)
import XMonad.Layout.Fullscreen ( FullscreenFull ()
                                , fullscreenFull
                                , fullscreenSupport
                                )
import XMonad.Layout.MultiToggle ( (??)
                                 , EOT (EOT)
                                 , Transformer (transform)
                                 , mkToggle
                                 , mkToggle1
                                 )
import XMonad.Layout.LayoutHints (layoutHintsToCenter)
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.ResizableTile (ResizableTall (ResizableTall))
import XMonad.Layout.BoringWindows (boringAuto)
import XMonad.Layout.LayoutModifier (ModifiedLayout ())
import XMonad.Layout.CenteredIfSingle (centeredIfSingle)
import XMonad.Layout.DraggingVisualizer (draggingVisualizer)
import XMonad.Layout.MultiToggle.Instances (StdTransformers ( NBFULL
                                                            , NOBORDERS
                                                            )
                                           )

import App.Projects (at)

import Theme.Theme ( basebg
                   , basefg
                   , base03
                   , base04
                   , base12
                   , base18
                   , myBigFont
                   )

-- | TODO: Write/Find a function to rotate the binary tree by 90° with a single keypress in the BSP Layout

layout = layoutOpts $ tall ||| full where
    layoutOpts = cutWords 5
                   . addTabbed
                   . avoidStruts
                   . toggleHints
                   . customSpacing
                   . hiddenWindows
                   . toggleBorders
                   . toggleCentered
                   . draggingVisualizer
                   . onW 3 hints
                   . onW 10 full

    full = rn "full" . noBorders $ Full
    hints = rn "tall hints" . layoutHintsToCenter $ tall
    tall = rn "tall" . customGaps . reflectToggle $ ResizableTall 1 (1/100) goldRatio []

    goldRatio = 2/(1+toRational (sqrt 5 :: Double))

    onW x = onWorkspace (at x)
    reflectToggle = mkToggle1 REFLECTX
    toggleHints = mkToggle1 ToggleHints
    toggleCentered = mkToggle1 ToggleCentered
    toggleBorders = mkToggle $ NBFULL ?? NOBORDERS ?? EOT
    addTabbed = boringAuto . addTabsBottom EmptyShrinker tabTheme . subLayout [] Simplest

data Gaps' = Gaps' {u, d, l, r :: Integer}

gap :: Num p => p
gap = 6

gs :: Gaps'
gs = Gaps' {u = gap, d = gap, l = gap, r = gap}

customGaps :: l a -> ModifiedLayout Gaps l a
customGaps = gaps . zip [U, D, L, R] $ repeat 0

customSpacing :: l a -> ModifiedLayout Spacing l a
customSpacing = spacingRaw False (Border (u gs) 0 (r gs) 0) True (Border 0 (d gs) 0 (l gs)) True

rn :: String -> l a -> ModifiedLayout Rename l a
rn s = renamed [Replace s]

cutWords :: Int -> l a -> ModifiedLayout Rename l a
cutWords i = renamed [CutWordsLeft i]

data EmptyShrinker = EmptyShrinker deriving (Read, Show)
instance Shrinker EmptyShrinker where
    shrinkIt _ _ = []

data ToggleHints = ToggleHints deriving (Eq, Read, Show)
instance Transformer ToggleHints Window where
    transform ToggleHints x k = k (layoutHintsToCenter x) (const x)

data ToggleCentered = ToggleCentered deriving (Eq, Read, Show)
instance Transformer ToggleCentered Window where
    transform ToggleCentered x k = k (centeredIfSingle 0.75 1 x) (const x)

tabTheme :: Theme
tabTheme = def { decoHeight = 8
               , decoWidth = maxBound
               , activeColor = base04
               , urgentColor = base03
               , activeBorderWidth = 0
               , urgentBorderWidth = 0
               , inactiveColor = base18
               , inactiveBorderWidth = 0
               , activeTextColor = base04
               , urgentTextColor = base03
               , inactiveTextColor = basebg
               , activeBorderColor = base04
               , urgentBorderColor = base03
               , inactiveBorderColor = basebg
               }

emConfig :: EasyMotionConfig
emConfig = def { bgCol = basebg
               , txtCol = basefg
               , emFont = myBigFont
               , borderCol = base12
               , borderPx = 5
               , overlayF = fixedSize (350 :: Int) (100 :: Int)
               , maxChordLen = -1
               , cancelKey = xK_Escape
               , sKeys = AnyKeys [ xK_a, xK_s, xK_d
                                 , xK_f, xK_h, xK_j
                                 , xK_k, xK_l, xK_c
                                 ]
               }

fullscreenCustom :: LayoutClass l Word64 => XConfig l -> XConfig (ModifiedLayout FullscreenFull (ModifiedLayout (ConfigurableBorder Ambiguity) (ModifiedLayout FullscreenFull l)))
fullscreenCustom c = fullscreenSupport c { layoutHook = lessBorders Never . fullscreenFull . layoutHook $ c }

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
