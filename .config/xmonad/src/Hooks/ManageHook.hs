{- |
   Module : Hooks.ManageHook
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.ManageHook (windowManager) where

import Prelude ( (.)
               , (/)
               , (>>=)
               , Bool (False)
               , String ()
               , Rational ()
               , mconcat
               )

import Control.Monad (liftM2)

import XMonad ( (=?)
              , (-->)
              , (<&&>)
              , Query ()
              , ManageHook ()
              , ask
              , def
              , doF
              , doFloat
              , className
              , manageHook
              , stringProperty
              )
import XMonad.StackSet ( RationalRect (RationalRect)
                       , sink
                       , view
                       , shift
                       )

import XMonad.Actions.Sift (siftUp)
import XMonad.Actions.SpawnOn (manageSpawn)

import XMonad.Hooks.ManageHelpers ( doFocus
                                  , isDialog
                                  , doFloatDep
                                  , transience'
                                  , isInProperty
                                  , doCenterFloat
                                  )
import XMonad.Hooks.InsertPosition ( Focus (Newer)
                                   , Position (Below)
                                   , insertPosition
                                   )

import XMonad.Layout.NoBorders (hasBorder)

import XMonad.Util.NamedScratchpad (namedScratchpadManageHook)

import App.Projects (at)
import App.Scratchpad (scratchpads)

windowManager :: ManageHook
windowManager = mconcat [ tileBelow
                        , transience'
                        , manageSpawn
                        , manageHook def
                        , isRole =? "About" --> doCenterFloat
                        , namedScratchpadManageHook scratchpads
                        , isRole =? "pop-up" --> forceCenterFloat
                        , className =? "Sxiv" --> hasBorder False
                        , className =? "Picker" --> doCenterFloat
                        , isName =? "Library" --> forceCenterFloat
                        , className =? "Places" --> forceCenterFloat
                        , className =? "New VM" --> forceCenterFloat
                        , className =? "Ripcord" --> shiftFocus (at 5)
                        , className =? "discord" --> shiftFocus (at 5)
                        , className =? "Filezilla" --> shiftFocus (at 6)
                        , className =? "qutebrowser" --> shiftFocus (at 2)
                        , className =? "Signal Beta" --> shiftFocus (at 5)
                        , isDialog --> mconcat [doF siftUp, doCenterFloat]
                        , isName =? "Picture-in-Picture" --> forceCenterFloat
                        , className =? "TelegramDesktop" --> shiftFocus (at 5)
                        , className =? "mpv" --> mconcat [doF siftUp, unFloat]
                        , isRole =? "GtkFileChooserDialog" --> forceCenterFloat
                        , className =? "st-float" --> mconcat [doFocus, doFloat]
                        , className =? "Ripcord" <&&> isName =? "Preferences" --> forceCenterFloat
                        , className =? "help" --> mconcat [forceCenterFloat, doF siftUp, hasBorder False]
                        , isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_SPLASH" --> doCenterFloat
                        ]

shiftFocus :: String -> ManageHook
shiftFocus = doF . liftM2 (.) view shift

unFloat :: ManageHook
unFloat = ask >>= doF . sink

isName :: Query String
isName = stringProperty "WM_NAME"

isRole :: Query String
isRole = stringProperty "WM_WINDOW_ROLE"

tileBelow :: ManageHook
tileBelow = insertPosition Below Newer

forceCenterFloat :: ManageHook
forceCenterFloat = doFloatDep center where
    center :: RationalRect -> RationalRect
    center _ = RationalRect w h x y

    w, h, x, y :: Rational
    w = 1/6
    h = 1/6
    x = 2/3
    y = 2/3

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
