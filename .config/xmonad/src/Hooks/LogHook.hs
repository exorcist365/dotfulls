{- |
   Module : Hooks.LogHook
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.LogHook ( logger
                     , statusBar
                     ) where

import Prelude ( ($)
               , (.)
               , (+)
               , (-)
               , (<)
               , (<>)
               , (==)
               , (<$>)
               , (>>=)
               , Num ((+))
               , Int ()
               , String ()
               , show
               , const
               , maybe
               , return
               , length
               , concat
               , mconcat
               , otherwise
               )

import Data.Char (toLower)
import Data.List (isSuffixOf)

import XMonad ( (-->)
              , X ()
              , def
              , gets
              , logHook
              , windowset
              , description
              )
import XMonad.StackSet ( peek
                       , stack
                       , layout
                       , current
                       , workspace
                       , integrate'
                       )

import XMonad.Actions.CopyWindow (copiesPP)

import XMonad.Hooks.StatusBar ( StatusBarConfig ()
                              , statusBarProp
                              , xmonadPropLog'
                              )
import XMonad.Hooks.DynamicLog ( PP ( ppSort
                                    , ppTitle
                                    , ppLayout
                                    , ppUrgent
                                    , ppHidden
                                    , ppExtras
                                    , ppCurrent
                                    , ppHiddenNoWindows
                                    )
                               , wrap
                               , shorten'
                               , filterOutWsPP
                               )
import XMonad.Hooks.FadeWindows ( FadeHook ()
                                , opaque
                                , isFloating
                                , isUnfocused
                                , fadeWindowsLogHook
                                )
import XMonad.Hooks.ManageHelpers (isDialog)

import XMonad.Util.NamedWindows (getName)
import XMonad.Util.NamedScratchpad (scratchpadWorkspaceTag)
import XMonad.Util.WorkspaceCompare (getSortByIndex)
import XMonad.Util.ClickableWorkspaces (clickablePP)

import Theme.Theme ( basefg
                   , base01
                   , base02
                   , base03
                   , base04
                   , base05
                   , base06
                   , base09
                   , base12
                   , base14
                   , base16
                   , base18
                   )

logger :: X ()
logger = mconcat [ logTitle
                 , logLayout
                 , logWindows
                 , logHook def
                 , fadeWindowsLogHook fadeHook
                 ]

logLayout :: X ()
logLayout = do winset <- gets windowset

               xmonadPropLog' "_XMONAD_LAYOUT" . layoutParse . description . layout . workspace . current $ winset

fmtLStr :: String -> String -> String -> String
fmtLStr c i n = concat ["<fn=3><fc=", c, ":0>", i, "</fc></fn><fc=", basefg, ":0> ", n, "</fc>"]

layoutParse :: String -> String
layoutParse s | s == "tall" = fmtLStr base05 "|!" s
              | s == "full" = fmtLStr base05 "[ ]" s
              | "hints" `isSuffixOf` s = fmtLStr base05 "| |" "video"
              | otherwise = fmtLStr base12 "??!" s

logTitle :: X ()
logTitle = do winset <- gets windowset
              t <- maybe (return "") (((toLower <$>) . shorten' "~" 105 . show <$>) . getName) . peek $ winset

              let title :: String
                  title = concat $ case t of
                               "" -> [""]
                               _ -> [ "<fn=1><fc="
                                    , base18
                                    , ":0>| </fc></fn><fn=3><fc="
                                    , base14
                                    , ":0>"
                                    , t
                                    , "</fc></fn>"
                                    ]

              xmonadPropLog' "_XMONAD_TITLE" title

logWindows :: X ()
logWindows = do count <- gets $ length . integrate' . stack . workspace . current . windowset

                xmonadPropLog' "_XMONAD_WINDOWS" . fmtWStr . show' $ count - 1

show' :: Int -> String
show' i | i < -1 = show i
        | otherwise = show (i + 1)

fmtWStr :: String -> String
fmtWStr c = concat ["<fn=0><fc=", base01, ":0>", c, "</fc></fn>"]

fadeHook :: FadeHook
fadeHook = mconcat [ opaque
                   , isDialog --> opaque
                   , isFloating --> opaque
                   , isUnfocused --> opaque
                   ]

statusBar :: StatusBarConfig
statusBar = do let urgent, focused :: String -> String
                   urgent = wrap ("<fc=" <> base06 <> ":0>!</fc><fn=3><fc=" <> base03 <> ":0>") "</fc></fn>"
                   focused = wrap ("<fc=" <> base09 <> ":0>@</fc><fn=3><fc=" <> base02 <> ":0>") "</fc></fn>"

                   barPP :: PP
                   barPP = def
                       { ppExtras = []
                       , ppUrgent = urgent
                       , ppTitle = const ""
                       , ppLayout = const ""
                       , ppCurrent = focused
                       , ppSort = getSortByIndex
                       , ppHiddenNoWindows = const ""
                       , ppHidden = wrap ("<fc=" <> base16 <> ":0>*</fc><fc=" <> base12 <> ":0>") "</fc>"
                       }

               statusBarProp "xmobar" $ clickablePP (filterOutWsPP [scratchpadWorkspaceTag] barPP)
                                            >>= copiesPP (wrap ("<fc=" <> base05 <> ":0>#</fc><fc=" <> base04 <> ":0>") "</fc>")

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
