{- |
   Module : Hooks.HandleEventHook
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.HandleEventHook (handler) where

import Prelude ( ($)
               , (<$>)
               , Bool (True)
               , String ()
               , return
               , mconcat
               )

import Data.Monoid (All ())

import XMonad ( (=?)
              , X ()
              , Event ()
              , def
              , className
              , handleEventHook
              )

import XMonad.Actions.UpdateFocus (focusOnMouseMove)

import XMonad.Hooks.FadeWindows (fadeWindowsEventHook)
import XMonad.Hooks.WindowSwallowing (swallowEventHookSub)

import XMonad.Layout.NoBorders (borderEventHook)
import XMonad.Layout.LayoutHints (hintsEventHook)

handler :: Event -> X All
handler = mconcat [ mconcat $ swallower <$> ["St"]
                  , hintsEventHook
                  , borderEventHook
                  , focusOnMouseMove
                  , handleEventHook def
                  , fadeWindowsEventHook
                  ]

swallower :: String -> Event -> X All
swallower prog = swallowEventHookSub (className =? prog) (return True)

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
