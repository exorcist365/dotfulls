{- |
   Module : Main (for xmobar)
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Main (main) where

import Prelude ( ($)
               , (.)
               , (<>)
               , (!!)
               , IO ()
               , Int ()
               , Bool ( True
                      , False
                      )
               , Read ()
               , Show ()
               , String ()
               , show
               , return
               , concat
               )

import System.FilePath ((</>))

import Xmobar

import App.Alias ( (+++)
                 , term
                 , binDir
                 )

import Theme.Theme ( basebg
                   , basefg
                   , base00
                   , base08
                   , base01
                   , base09
                   , base02
                   , base10
                   , base03
                   , base11
                   , base04
                   , base12
                   , base05
                   , base13
                   , base06
                   , base14
                   , base07
                   , base15
                   , base16
                   , base17
                   , base18
                   , myFont
                   , myBoldFont
                   )

data Record = Record deriving (Read, Show)
instance Exec Record where
    alias _ = "rec"
    run _ = return . inFG 12 . inFont 0 $ "<action=`" <> binDir </> "record`>rec</action>"

sep :: String
sep = inFG 18 $ inFont 1 "|"

inFont :: Int -> String -> String
inFont i t = "<fn=" <> show i <> ">" <> t <> "</fn>"

inFG :: Int -> String -> String
inFG c t = "<fc=" <> base c <> "," <> base 19 <> ":0>" <> t <> "</fc>"

-- inBG :: Int -> String -> String
-- inBG c t = "<fc=" <> base 19 <> "," <> base c <> ":0>" <> t <> "</fc>"

-- inFGAndBG :: Int -> Int -> String -> String
-- inFGAndBG f b t = "<fc=" <> base f <> "," <> base b <> ":0>" <> t <> "</fc>"

base :: Int -> String
base = ([ base00
        , base01
        , base02
        , base03
        , base04
        , base05
        , base06
        , base07
        , base08
        , base09
        , base10
        , base11
        , base12
        , base13
        , base14
        , base15
        , base16
        , base17
        , base18
        , basebg
        , basefg
        ] !!)

volActions, mpdActions, kbdActions, wifiActions, layoutActions, weatherActions :: String -> String
mpdActions s = concat [ "<action=`" <> binDir </> "music" +++ "toggle` button=1>"
                      , "<action=`" <> binDir </> "music" +++ "stop` button=2>"
                      , "<action=`xdotool key super+m` button=3>"
                      , "<action=`" <> binDir </> "music" +++ "next` button=4>"
                      , "<action=`" <> binDir </> "music" +++ "prev` button=5>"
                      , s
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      ]
kbdActions x = concat [ "<action=`xdotool key Shift_L+Shift_R` button=1>"
                      , x
                      , "</action>"
                      ]
volActions k = concat [ "<action=`" <> binDir </> "vol" +++ "mute` button=1>"
                      , "<action=`pgrep -x pulsemixer ||" +++ term +++ "-e pulsemixer` button=3>"
                      , "<action=`" <> binDir </> "vol" +++ "+` button=4>"
                      , "<action=`" <> binDir </> "vol" +++ "-` button=5>"
                      , k
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      ]
wifiActions s = concat [ "<action=`pgrep -x iwctl ||" +++ term +++ "-e iwctl` button=1>"
                       , s
                       , "</action>"
                       ]
layoutActions x = concat [ "<action=`xdotool key super+space` button=1>"
                         , "<action=`xdotool key super+control+space` button=3>"
                         , x
                         , "</action>"
                         , "</action>"
                         ]
weatherActions s = concat [ "<action=`" <> binDir </> "open" +++ "https://sinoptik.bg` button=1>"
                          , s
                          , "</action>"
                          ]
runCommands :: [Runnable]
runCommands = [ Run UnsafeXMonadLog
              , Run $ UnsafeNamedXPropertyLog "_XMONAD_WINDOWS" "window"
              , Run $ UnsafeNamedXPropertyLog "_XMONAD_LAYOUT" "layout"
              , Run $ UnsafeNamedXPropertyLog "_XMONAD_TITLE" "title"
              , Run Record
              , Run $ MPD [ "-t"
                          , inFG 8 (inFont 0 "[mpd]") <> ":" +++ "<statei>"
                          , "--"
                          , "-P"
                          , inFG 2 (inFont 0 "playing") <> ":" +++ inFG 16 (inFont 0 "<title>")
                          , "-Z"
                          , inFG 3 $ inFont 0 "paused"
                          , "-S"
                          , inFG 1 $ inFont 0 "stopped"
                          ] 10
              , Run $ Alsa "default" "Master" [ "-t"
                                              , inFG 8 (inFont 0 "[alsa]") <> ":" +++ "<status>"
                                              , "--"
                                              , "-C"
                                              , base 20
                                              , "-c"
                                              , base 20
                                              , "-O"
                                              , " <volume>%"
                                              , "-o"
                                              , inFG 13 (inFont 0 "muted")
                                              , "-H"
                                              , "75"
                                              , "-L"
                                              , "10"
                                              , "-h"
                                              , inFG 1 (inFont 3 "loud") <> ":"
                                              , "-m"
                                              , inFG 10 (inFont 0 "normal") <> ":"
                                              , "-l"
                                              , inFG 12 (inFont 0 "silent") <> ":"
                                              ]
              , Run $ Kbd [ ("us", inFG 5 (inFont 0 "kb") <> ":" +++ "en")
                          , ("gr", inFG 5 (inFont 0 "kb") <> ":" +++ "gr")
                          , ("bg(phonetic)", inFG 5 (inFont 0 "kb") <> ":" +++ "bg")
                          ]
              , Run $ WeatherX "LBSF" [ ("", inFG 3 $ inFont 0 "clear")
                                      , ("fair", inFG 8 $ inFont 0 "fair")
                                      , ("clear", inFG 3 $ inFont 0 "clear")
                                      , ("sunny", inFG 3 $ inFont 0 "sunny")
                                      , ("cloudy", inFG 8 $ inFont 0 "cloudy")
                                      , ("obscured", inFG 8 $ inFont 0 "obscured")
                                      , ("overcast", inFG 8 $ inFont 0 "overcast")
                                      , ("mostly clear", inFG 12 $ inFont 0 "m clear")
                                      , ("mostly sunny", inFG 12 $ inFont 0 "m sunny")
                                      , ("partly sunny", inFG 8 $ inFont 0 "p sunny")
                                      , ("partly cloudy", inFG 8 $ inFont 0 "p cloudy")
                                      , ("mostly cloudy", inFG 8 $ inFont 0 "m cloudy")
                                      , ("considerable cloudiness", inFG 8 $ inFont 0 "cons cloudy")
                                      ] [ "-t"
                                        , "<skyConditionS>: <tempC>°C"
                                        , "-L"
                                        , "0"
                                        , "-H"
                                        , "30"
                                        , "-n"
                                        , base 2
                                        , "-h"
                                        , base 1
                                        , "-l"
                                        , base 12
                                        ] 6000
              , Run $ Wireless "wlan0" [ "-t"
                                       , inFG 4 (inFont 0 "net") <> ":" +++ "<ssid>"
                                       ] 3600
              , Run $ BatteryP ["BAT1"]
                               [ "-t"
                               , "<acstatus>"
                               , "-a"
                               , "notify-send -t 10000000 -u critical 'Battery is running out!'"
                               , "--"
                               , "-P"
                               , "-L"
                               , "-15"
                               , "-H"
                               , "-5"
                               , "-l"
                               , base 20
                               , "-m"
                               , base 20
                               , "-h"
                               , base 20
                               , "-p"
                               , base 20
                               , "-O"
                               , inFG 10 (inFont 3 "charging") <> ":" +++ "<left>"
                               , "-o"
                               , inFG 1 (inFont 3 "discharging") <> ":" +++ "<left>/<timeleft>"
                               , "-i"
                               , inFG 2 (inFont 0 "charged")
                               , "-A"
                               , "3"
                               ] 600
              , Run $ Date (inFG 9 (inFont 0 "date") <> ":" +++ "%d.%m") "date" 36000
              , Run $ Date (inFG 3 (inFont 0 "time") <> ":" +++ "%H:%M") "time" 600
              ]

config :: Config
config = defaultConfig { font = additionalFonts config !! 3
                       , additionalFonts = [ "xft:Exosevka:pixelsize=14:style=Regular:antialias=true:autohint=false"
                                           , "xft:Font Awesome 5 Pro:style=Solid:pixelsize=10:antialias=true:autohint=false"
                                           , myBoldFont <> "," <> "Noto Color Emoji:style=Regular:pixelsize=14:antialias=true:hinting=false"
                                           , myFont <> "," <> "Noto Color Emoji:style=Regular:pixelsize=14:antialias=true:hinting=false"
                                           , "xft:Noto Color Emoji:pixelsize=14:style=Regular:antialias=true:autohint=false"
                                           ]
                       , textOffset = 20
                       , textOffsets = [20, 20, textOffset config, textOffset config, 20]
                       , borderColor = base 19
                       , borderWidth = 0
                       , border = NoBorder
                       , bgColor = base 19
                       , fgColor = base 20
                       , alpha = 255
                       , position = BottomH 30
                       , iconOffset = 0
                       , lowerOnStart = True
                       , pickBroadest = False
                       , persistent = True
                       , hideOnStart = False
                       , allDesktops = True
                       , overrideRedirect = True
                       , commands = runCommands
                       , sepChar = "~"
                       , alignSep = "}{"
                       , template = "" +++ "~UnsafeXMonadLog~" +++ sep +++
                                    "~window~" +++ sep +++
                                    layoutActions "~layout~" +++
                                    "~title~" +++
                                    "}{" +++
                                    "~rec~" +++ sep +++
                                    mpdActions "~mpd~" +++ sep +++
                                    volActions "~alsa:default:Master~" +++ sep +++
                                    kbdActions "~kbd~" +++ sep +++
                                    weatherActions "~LBSF~" +++ sep +++
                                    wifiActions "~wlan0wi~" +++ sep +++
                                    "~battery~" +++ sep +++
                                    "~date~" +++ sep +++
                                    "~time~" +++ ""
                       }


main :: IO ()
main = xmobar config
