{-# LANGUAGE LambdaCase #-}

{- |
   Module : Config.Prompts
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Config.Prompts ( hacksaw
                      , promptTheme
                      , powerPrompt
                      , mediaPrompt
                      , warningTheme
                      , unicodeTheme
                      , volumePrompt
                      , criticalTheme
                      , appImagePrompt
                      , termLaunchPrompt
                      ) where

import Prelude ( ($)
               , (.)
               , (/)
               , (<>)
               , (/=)
               , (>>=)
               , (<$>)
               , IO ()
               , Int ()
               , Bool ( True
                      , False
                      )
               , Maybe ( Just
                       , Nothing
                       )
               , Double ()
               , String ()
               , show
               , read
               , words
               , concat
               , filter
               , unwords
               , truncate
               )


import Data.Char (toLower)

import System.Exit (exitSuccess)
import System.FilePath ((</>))
import System.Process ( CreateProcess (cwd)
                      , proc
                      , createProcess
                      )
import System.Directory (listDirectory)

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)

import Graphics.X11.Types (xK_less)

import XMonad ( X ()
              , spawn
              , restart
              )

import XMonad.Prompt ( XPConfig ( font
                                , height
                                , sorter
                                , bgColor
                                , fgColor
                                , bgHLight
                                , fgHLight
                                , position
                                , defaultText
                                , historySize
                                , borderColor
                                , maxComplRows
                                , autoComplete
                                , promptKeymap
                                , completionKey
                                , historyFilter
                                , defaultPrompter
                                , maxComplColumns
                                , searchPredicate
                                , alwaysHighlight
                                , promptBorderWidth
                                , showCompletionOnTab
                                , complCaseSensitivity
                                )
                     , XPPosition (Bottom)
                     , ComplCaseSensitivity (CaseInSensitive)
                     , def
                     , defaultXPKeymap
                     , mkComplFunFromList
                     , deleteAllDuplicates
                     )
import XMonad.Prompt.Shell (getCommands)
import XMonad.Prompt.Input ( (?+)
                           , inputPromptWithCompl
                           )
import XMonad.Prompt.FuzzyMatch ( fuzzySort
                                , fuzzyMatch
                                )

import XMonad.Util.Run (runProcessWithInput)
import XMonad.Util.XSelection (getSelection)

import App.Alias ( term
                 , binDir
                 , dataHome
                 , spawnList
                 , dataDrive
                 )

import Debug.Debug (debugNotif)

import Theme.Theme ( basebg
                   , basefg
                   , base01
                   , base02
                   , base03
                   , base09
                   , base06
                   , myFont
                   , myBoldFont
                   )

promptTheme :: XPConfig
promptTheme = def { height = 30
                  , font = myFont
                  , position = Bottom
                  , bgColor = basebg
                  , fgColor = basefg
                  , bgHLight = base09
                  , fgHLight = basebg
                  , sorter = fuzzySort
                  , historySize = 8192
                  , borderColor = base02
                  , promptBorderWidth = 2
                  , maxComplRows = Just 10
                  , autoComplete = Nothing
                  , defaultPrompter = (toLower <$>)
                  , alwaysHighlight = True
                  , maxComplColumns = Just 10
                  , showCompletionOnTab = False
                  , completionKey = (0, xK_less)
                  , searchPredicate = fuzzyMatch
                  , promptKeymap = defaultXPKeymap
                  , historyFilter = deleteAllDuplicates
                  , complCaseSensitivity = CaseInSensitive
                  }

unicodeTheme :: XPConfig
unicodeTheme = promptTheme { font = myFont <> "," <> "Noto Color Emoji:style=Regular:pixelsize=14:antialias=true:hinting=false" }

warningTheme :: XPConfig
warningTheme = promptTheme { bgColor = base03
                           , fgColor = basebg
                           , font = myBoldFont
                           , promptBorderWidth = 0
                           , promptKeymap = defaultXPKeymap
                           }

criticalTheme :: XPConfig
criticalTheme = promptTheme { bgColor = base01
                            , fgColor = basebg
                            , font = myBoldFont
                            , promptBorderWidth = 0
                            , promptKeymap = defaultXPKeymap
                            }

volumePrompt :: XPConfig -> X ()
volumePrompt c = inputPromptWithCompl c "volume" (volumeCompletion c) ?+ \input ->
    spawnList [binDir </> "vol", input]

volumeCompletion :: XPConfig -> String -> IO [String]
volumeCompletion c = mkComplFunFromList c $ show <$> ([0..100] :: [Int])

appImagePrompt :: XPConfig -> X ()
appImagePrompt c = inputPromptWithCompl c "run" (appImageCompletion c) ?+ \input ->
    spawn $ dataHome </> "appimages" </> input

appImageCompletion :: XPConfig -> String -> IO [String]
appImageCompletion c s = do contents <- listDirectory $ dataHome </> "appimages"
                            mkComplFunFromList c contents s

mediaPrompt :: XPConfig -> X ()
mediaPrompt c = inputPromptWithCompl (c { defaultText = "music" }) "download as" (mediaCompletion c) ?+ \input ->
    getSelection >>= \sel ->
        case sel of
          ('h':'t':'t':'p':_) ->
              case input of
                "music" -> do ytdlCommand ytdlOptsAudio sel "Music"
                              debugNotif $ unwords ["Successfully downloaded", sel, "!"]
                "video" -> do ytdlCommand ytdlOptsVideo sel "Video"
                              debugNotif $ unwords ["Successfully downloaded", sel, "!"]
                _ -> debugNotif "Invalid action"
          _ -> debugNotif "Make sure to select a valid link first"

ytdlCommand :: (String -> [String]) -> String -> String -> X ()
ytdlCommand f x d = liftIO . void $ createProcess (proc "yt-dlp" (f x)) { cwd = Just $ dataDrive </> d }

ytdlOptsVideo, ytdlOptsAudio :: String -> [String]
ytdlOptsVideo x = [ "-i"
                  , "-f", "bestvideo"
                  , "--console-title"
                  , "--add-metadata"
                  , "--embed-thumbnail"
                  , "--output=" <> "'%(title)s.%(ext)s'"
                  , "--ppa \"EmbedThumbnail+ffmpeg_o:-c:v png -vf crop=\"'if(gt(ih,iw),iw,ih)':'if(gt(iw,ih),ih,iw)'\"\""
                  , x
                  ]
ytdlOptsAudio x = [ "-i"
                  , "-x"
                  , "-f", "bestaudio"
                  , "--console-title"
                  , "--audio-quality=0"
                  , "--audio-format=mp3"
                  , "--embed-thumbnail"
                  , "--parse-metadata 'artist:%(album_artist)s'"
                  , "--parse-metadata 'playlist_index:%(track_number)s'"
                  , "--add-metadata"
                  , "--output=" <> "'%(title)s.%(ext)s'"
                  , "--ppa \"EmbedThumbnail+ffmpeg_o:-c:v png -vf crop=\"'if(gt(ih,iw),iw,ih)':'if(gt(iw,ih),ih,iw)'\"\""
                  , x
                  ]

mediaCompletion :: XPConfig -> String -> IO [String]
mediaCompletion c = mkComplFunFromList c ["music", "video"]

powerPrompt :: XPConfig -> X ()
powerPrompt c = inputPromptWithCompl (c { defaultText = "power off" }) "execute" (powerCompletion c) ?+ \case
      "exit" -> liftIO exitSuccess
      "recompile" -> restart "xmonad" True
      "reboot" -> spawnList ["systemctl", "reboot"]
      "power off" -> spawnList ["systemctl", "poweroff"]
      "lock" -> spawnList ["xset", "s", "activate", "&&", binDir </> "locker", "&"]
      _ -> debugNotif "Invalid action"

powerCompletion :: XPConfig -> String -> IO [String]
powerCompletion c = mkComplFunFromList c ["exit", "recompile", "power off", "reboot", "lock"]

hacksaw :: X [Double]
hacksaw = ((<$>) . (<$>)) read words . filter (/= '\n') <$> runProcessWithInput "hacksaw" [ "-f", "%w %h %x %y"
                                                                                          , "-g", "4"
                                                                                          , "-s", "4"
                                                                                          , "-r", "1"
                                                                                          , "-c", base06
                                                                                          ] []

termLaunchPrompt :: XPConfig -> X ()
termLaunchPrompt c = inputPromptWithCompl c "launch" (termCompletion c) ?+ \input ->
    hacksaw >>= \[w, h, x, y] -> do
        let [g1, g2, g3, g4] = show . truncate <$> [w/8, h/18, x, y]
        spawnList [term, "-g", concat [g1, "x", g2, "+", g3, "+", g4], "-c", "st-float", "-e", input]

termCompletion :: XPConfig -> String -> IO [String]
termCompletion c s = getCommands >>= \cmd -> mkComplFunFromList c cmd s

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
