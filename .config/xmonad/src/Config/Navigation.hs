{- |
   Module : Config.Navigation
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Config.Navigation (navConf) where

import XMonad.Actions.Navigation2D ( Navigation2DConfig (defaultTiledNavigation)
                                   , def
                                   , hybridOf
                                   , sideNavigation
                                   , centerNavigation
                                   )

navConf :: Navigation2DConfig
navConf = def { defaultTiledNavigation = hybridOf sideNavigation centerNavigation }

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
