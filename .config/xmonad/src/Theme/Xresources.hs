{- |
   Module : Theme.Xresources
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Theme.Xresources ( xprop
                        , splitAtColon
                        , splitAtTrimming
                        ) where

import Prelude ( ($)
               , (.)
               , (==)
               , (<$>)
               , IO ()
               , Int ()
               , Maybe ()
               , String ()
               , fst
               , snd
               , tail
               , lines
               , splitAt
               , dropWhile
               )

import Data.List ( find
                 , elemIndex
                 , dropWhileEnd
                 )
import Data.Char (isSpace)
import Data.Maybe ( mapMaybe
                  , fromMaybe
                  )
import Data.Bifunctor (bimap)

import System.IO.Unsafe (unsafeDupablePerformIO)

import XMonad.Util.Run (runProcessWithInput)

xProperty :: String -> IO String
xProperty k = fromMaybe "" . findValue k <$> runProcessWithInput "xrdb" ["-query"] []

findValue :: String -> String -> Maybe String
findValue k x = snd <$> find ((== k) . fst) (mapMaybe splitAtColon (lines x))

splitAtColon :: String -> Maybe (String, String)
splitAtColon s = splitAtTrimming s <$> elemIndex ':' s

splitAtTrimming :: String -> Int -> (String, String)
splitAtTrimming s i = bimap trim (trim . tail) $ splitAt i s

trim, xprop :: String -> String
trim = dropWhileEnd isSpace . dropWhile isSpace
xprop = unsafeDupablePerformIO . xProperty

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
