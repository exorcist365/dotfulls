{-# LANGUAGE ScopedTypeVariables #-}

{- |
   Module : Theme.Theme
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Theme.Theme ( basebg
                   , basefg
                   , base00
                   , base08
                   , base01
                   , base09
                   , base02
                   , base10
                   , base03
                   , base11
                   , base04
                   , base12
                   , base05
                   , base13
                   , base06
                   , base14
                   , base07
                   , base15
                   , base16
                   , base17
                   , base18
                   , myFont
                   , myBigFont
                   , myBoldFont
                   , myItalicFont
                   ) where

import Prelude (String ())

import Theme.Xresources (xprop)

basebg :: String = xprop "*.background"
basefg :: String = xprop "*.foreground"
base00 :: String = xprop "*.color0"
base08 :: String = xprop "*.color8"
base01 :: String = xprop "*.color1"
base09 :: String = xprop "*.color9"
base02 :: String = xprop "*.color2"
base10 :: String = xprop "*.color10"
base03 :: String = xprop "*.color3"
base11 :: String = xprop "*.color11"
base04 :: String = xprop "*.color4"
base12 :: String = xprop "*.color12"
base05 :: String = xprop "*.color5"
base13 :: String = xprop "*.color13"
base06 :: String = xprop "*.color6"
base14 :: String = xprop "*.color14"
base07 :: String = xprop "*.color7"
base15 :: String = xprop "*.color15"
base16 :: String = xprop "*.color16"
base17 :: String = xprop "*.color17"
base18 :: String = xprop "*.color18"

myFont, myBigFont, myBoldFont, myItalicFont :: String
myFont = xprop "xmonad.font"
myBigFont = xprop "xmonad.font.big"
myBoldFont = xprop "xmonad.font.bold"
myItalicFont = xprop "xmonad.font.italic"

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
