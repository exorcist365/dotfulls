{- |
   Module : Bind.Mouse
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Bind.Mouse (mouse) where

import Prelude ( ($)
               , (.)
               , Num ()
               , Maybe (Just)
               )

import Data.Map ( Map ()
                , fromList
                )
import Data.Bits ((.|.))

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)

import System.FilePath ((</>))

import Graphics.X11.Types ( mod4Mask
                          , noModMask
                          , shiftMask
                          )

import Network.MPD ( next
                   , withMPD
                   , previous
                   )

import XMonad ( X ()
              , Button ()
              , Window ()
              , XConfig ()
              , ButtonMask ()
              , focus
              , mouseMoveWindow
              )

import XMonad.Actions.FloatSnap ( Direction2D ( U
                                              , D
                                              , R
                                              , L
                                              )
                                , afterDrag
                                , snapMagicMove
                                , snapMagicResize
                                )
import XMonad.Actions.FlexibleResize (mouseResizeWindow)
import XMonad.Actions.TiledWindowDragging (dragWindow)

import App.Alias ( binDir
                 , spawnList
                 )

dirs :: [Direction2D]
dirs = [U, D, R, L]

snap :: Num p => p
snap = 20

shft, super :: ButtonMask
super = mod4Mask
shft = shiftMask

mouse :: XConfig l -> Map (ButtonMask, Button) (Window -> X ())
mouse _ = fromList
    [ ((super, 1), \w -> do focus w
                            mouseMoveWindow w
                            afterDrag $ snapMagicMove (Just snap) (Just snap) w
      )
    , ((super .|. shft, 1), dragWindow)
    , ((super, 2), \w -> do focus w
                            mouseMoveWindow w
                            afterDrag $ snapMagicResize dirs (Just snap) (Just snap) w
      )
    , ((super, 3), \w -> do focus w
                            mouseResizeWindow w
                            afterDrag $ snapMagicResize dirs (Just snap) (Just snap) w
      )
    , ((shft, 8), \_ -> spawnList [binDir </> "vol", "-"])
    , ((shft, 9), \_ -> spawnList [binDir </> "vol", "+"])
    , ((noModMask, 8), \_ -> liftIO . void $ withMPD previous)
    , ((noModMask, 9), \_ -> liftIO . void $ withMPD next)
    ]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
