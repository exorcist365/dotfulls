{- |
   Module : Bind.Help
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Bind.Help (showHelp) where

import Prelude ( ($)
               , (.)
               , (<>)
               , (<$>)
               , String ()
               , show
               )

import Data.List (unlines)

import Control.Monad.IO.Class (liftIO)

import System.IO (writeFile)
import System.FilePath ((</>))

import XMonad (X ())

import App.Alias ( fst
                 , snd
                 , term
                 , editor
                 , cacheDir'
                 , spawnList
                 )

showHelp :: X ()
showHelp = do let file :: String
                  file = cacheDir' </> "keybinds.txt"

              liftIO . writeFile file $ unlines entryText <> parseHelp help <> unlines endText
              spawnList [term, "-c", "help", "-e", editor, "-c", show "nnoremap q ZQ", "-R", file]

entryText :: [String]
entryText = ["Press q to quit.", [], "Welcome to my custom build of XMonad, here are the keybinds for it.", []]

endText :: [String]
endText = [[], "For more keybinds open $XMONAD_CONFIG_HOME/src/Bind/KeyBoard.hs"]

parseHelp :: [(String, String)] -> String
parseHelp xs = unlines $ (((<> ": ") <$> fst) <> snd) <$> xs

help :: [(String, String)]
help = [ ("Super + e", "Open Terminal")
       , ("Super + w", "Kill the focused window")
       , ("Super + s", "Toggle the state of the focused window between floating and tiled")
       , ("Super + backspace", "Download the copied url if it's and image and put it in the wallpaper directory")
       , ("Super + grave", "Pick a file to upload to 0x0.st")
       , ("Super + n (d, l)", "Switch the colorscheme from dark to light and vice-versa")
       , ("Super + o", "Display prompt search engine options")
       , ("Super + x", "Open the power prompt")
       , ("Super + minus", "Set a random wallpaper from the wallpapers folder")
       , ("Super + F2", "Download the copied url if it's music and put it in the music folder")
       , ("Super + F3", "Pick a device and an image to flash to that device")
       , ("Super + F4", "Toggle night light on or off")
       , ("Super + Shift + r", "Select a rectangle to record")
       , ("Super + Shift + slash", "Open a notification showing the currently playing song")
       , ("Super + Control + left bracket", "Toggle the compositor on or off")
       , ("Alt + b", "Open the browser set by the $BROWSER variable")
       , ("Alt + c", "Select a rectangle to open a floating terminal in")
       , ("Alt + n", "Open the music player set by the $MUSIC variable")
       , ("Alt + f", "Open the file manager set by the $FM variable")
       , ("Alt + g", "Open this menu")
       , ("Alt + h", "Open the system monitor set by the $SYSMON variable")
       , ("Alt + l (d, s, t, n, f, g)", "Open discord, signal, telegram, nicotine+, filezilla or gimp depending on the key being pressed")
       , ("Alt + p", "Open pulsemixer")
       , ("Alt + s", "Select an area to take a screenshot of")
       , ("Alt + v", "Open virt-manager")
       , ("Alt + w", "Open the image viewer set by the $IMAGE variable in the wallpapers folder")
       , ("Alt + x", "Select a color and copy it to the clipboard")
       , ("Alt + z", "Open qutebrowser")
       , ("Alt + Shift + c", "Opens a floating terminal with the selected dimensions and program running inside")
       , ("Control + escape", "Toggle the state of the microphone between muted and unmuted")
       , ("Control + Shift + l", "Lock the screen")
       , ("Super + space", "Go to the next layout")
       , ("Super + b", "Toggle the state of the bar between hidden and shown")
       , ("Super + f", "Toggle the state of the focused window between fullscreen and tiled")
       , ("Super + i", "Untab the focused window and go to the next one")
       , ("Super + r", "Rotate the windows dependent on the layout")
       , ("Super + t", "Tab the focused window and the one after it")
       , ("Super + comma", "Increase the stack size by one window")
       , ("Super + period", "Decrease the stack size by one window")
       , ("Super + Shift + minus", "Decrease the gap size by 3")
       , ("Super + Shift + equal", "Increase the gap size by 3")
       , ("Super + Control + m", "Tabs and untabs all windows in the current workspace")
       , ("Super + Shift + space", "Reset the layout to the default one on all workspaces")
       , ("Super + Shift + b", "Show and hide the borders of all windows in the current workspace")
       , ("Super + Shift + f", "Select a window to tab the focused one with")
       , ("Super + Shift + i", "Untab all windows in the current workspace")
       , ("Super + Shift + m", "Toggle between the tiled and the monocle layout")
       , ("Super + Control + space", "Apply the layout on the current workspace to all workspaces")
       , ("Super + Control + t", "Toggle the window hints on or off on the current workspace")
       , ("Super + Control + minus", "Decrease the spacing on the current workspace by 1")
       , ("Super + Control + equal", "Increase the spacing on the current workspace by 1")
       , ("Super + Alt + space", "Refresh")
       , ("Super + Alt + y", "Toggle copy of the selected window to all workspaces")
       , ("Super + Alt + o", "Open selected url in mpv")
       , ("Print", "Take a fullscreen screenshot")
       , ("XF86AudioMute", "Mute the sound")
       , ("XF86AudioPlay", "Play the music")
       , ("XF86AudioPause", "Pause the music")
       , ("XF86AudioPrev", "Play the previous song")
       , ("XF86AudioNext", "Play the next song")
       , ("XF86AudioStop", "Stop playing")
       , ("XF86AudioLowerVolume", "Lower the playback volume by 5%")
       , ("XF86AudioRaiseVolume", "Raise the playback volume by 5%")
       , ("Super + KP_divide", "Play the current song only")
       , ("Super + KP_multiply", "Toggle repeat in mpd")
       , ("Super + Control + b", "Go back in the song by 5 (percent?/seconds?)")
       , ("Super + Control + f", "Go forward in the song by 5 (percent?/seconds?)")
       , ("Super + d", "Open the discord scratchpad")
       , ("Super + g", "Open the ghci scratchpad")
       , ("Super + m", "Open the music scratchpad")
       , ("Super + v", "Open the terminal scratchpad")
       , ("Super + z", "Open the file manager scratchpad")
       , ("Super + Control + c", "Open the calculator scratchpad")
       , ("Super + u", "Pick an emoji and copy it to clipboard")
       , ("Super + left bracket", "Pick a window to focus")
       , ("Super + right bracket", "Pick a window to bring on the current workspace")
       , ("Super + Shift + q", "Ask whether to quit XMonad")
       , ("Super + Shift + w", "Ask whether to close all windows in the current workspace")
       , ("Super + Alt + return", "Pick a program to open")
       , ("Super + Alt + l", "Save the current windowset to $XMONAD_DATA_HOME/xmonad.log")
       , ("Super + a", "Toggle the focused window state between hidden and shown")
       , ("Super + c", "Focus the next window in the current workspace")
       , ("Super + q", "Recompile and restart XMonad")
       , ("Super + y", "Tile the focused window if it's floating")
       , ("Super + backslash", "Toggle Resize Mode")
       , ("Super + semicolon", "Focus the previous tab")
       , ("Super + apostrophe", "Focus the next tab")
       , ("Super + Shift + c", "Focus the previous window")
       , ("Super + Shift + d", "Focus the master window")
       , ("Super + Shift + k", "Pick a window to kill")
       , ("Super + Control + w", "Use xkill to kill the focused window")
       , ("Control + m", "Make the focused window the master")
       , ("Control + semicolon", "Move the focused tab up")
       , ("Control + apostrophe", "Move the focused tab down")
       , ("Super + return", "Repeat the previous keybind")
       ]
