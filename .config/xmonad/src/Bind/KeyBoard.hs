{-# LANGUAGE LambdaCase #-}

{- |
   Module : Bind.KeyBoard
   Copyright : (c) 2020, 2021, 2022 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Bind.KeyBoard (mappings) where

-- {{{

import Prelude ( ($)
               , (.)
               , (/)
               , (==)
               , (<>)
               , (*>)
               , (<$>)
               , (>>=)
               , Int ()
               , Num ((+))
               , Bool ( True
                      , False
                      )
               , Maybe (Just)
               , String ()
               , Functor ()
               , id
               , zip
               , show
               , const
               , return
               , concat
               , reverse
               , truncate
               , subtract
               )

import Data.Map ( Map ()
                , member
                , lookup
                , toList
                , fromList
                )
import Data.Bits ((.|.))
import Data.Bifunctor ( Bifunctor ()
                      , second
                      )

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)

import System.Exit (exitSuccess)
import System.FilePath ((</>))

import Network.MPD ( next
                   , stop
                   , repeat
                   , toggle
                   , single
                   , withMPD
                   , seekCur
                   , previous
                   )

import Graphics.X11.Types ( ButtonMask (), KeySym (), Window()
                          , xK_a, xK_b, xK_c
                          , xK_d, xK_e, xK_f
                          , xK_g, xK_h, xK_i
                          , xK_j, xK_k, xK_l
                          , xK_m, xK_n, xK_o
                          , xK_p, xK_q, xK_r
                          , xK_s, xK_t, xK_u
                          , xK_v, xK_w, xK_x
                          , xK_y, xK_z
                          , xK_0, xK_1, xK_9
                          , xK_slash, xK_equal
                          , xK_F2, xK_F3, xK_F4
                          , xK_Print, xK_BackSpace
                          , xK_period, xK_apostrophe
                          , xK_space, xK_comma, xK_grave
                          , xK_KP_Up, xK_KP_End, xK_KP_Down
                          , xK_Up, xK_Right, xK_Left, xK_Down
                          , xK_KP_Left, xK_KP_Home, xK_KP_Divide
                          , xK_KP_Right, xK_KP_Insert, xK_KP_Page_Up
                          , mod1Mask, shiftMask, controlMask, mod4Mask, noModMask
                          , xK_semicolon, xK_bracketleft, xK_bracketright, xK_Return, xK_backslash
                          , xK_KP_Multiply, xK_KP_Page_Down, xK_KP_Begin, xK_Escape, xK_minus, xK_Tab
                          )
import Graphics.X11.ExtraTypes.XF86 ( xF86XK_AudioMute, xF86XK_AudioPlay, xF86XK_AudioPrev, xF86XK_Launch1
                                    , xF86XK_AudioNext, xF86XK_AudioStop, xF86XK_MonBrightnessUp, xF86XK_Tools
                                    , xF86XK_AudioLowerVolume, xF86XK_AudioRaiseVolume, xF86XK_MonBrightnessDown
                                    )

import XMonad ( X ()
              , Resize ( Expand
                       , Shrink
                       )
              , Rectangle ( Rectangle
                          , rect_x
                          , rect_y
                          , rect_width
                          , rect_height
                          )
              , Message ()
              , XConfig (layoutHook)
              , IncMasterN (IncMasterN)
              , SomeMessage (SomeMessage)
              , ChangeLayout (NextLayout)
              , LayoutMessages (ReleaseResources)
              , gets
              , asks
              , spawn
              , config
              , restart
              , windows
              , refresh
              , whenJust
              , setLayout
              , recompile
              , windowset
              , killWindow
              , sendMessage
              , withFocused
              , withWindowSet
              , handleMessage
              , withUnfocused
              , getDirectories
              , broadcastMessage
              )
import XMonad.StackSet ( Screen (Screen)
                       , StackSet (StackSet)
                       , RationalRect (RationalRect)
                       , sink
                       , float
                       , swapUp
                       , layout
                       , hidden
                       , current
                       , visible
                       , focusUp
                       , focusUp'
                       , floating
                       , swapDown
                       , workspace
                       , focusDown
                       , focusDown'
                       , swapMaster
                       , focusMaster
                       )

import XMonad.Actions.Submap (visualSubmap)
import XMonad.Actions.Search ( SearchEngine ()
                             , imdb
                             , hoogle
                             , amazon
                             , youtube
                             , wayback
                             , stackage
                             , thesaurus
                             , wikipedia
                             , duckduckgo
                             , vocabulary
                             , promptSearch
                             , searchEngine
                             )
import XMonad.Actions.WithAll (killAll)
import XMonad.Actions.FloatKeys ( ChangeDim ()
                                , keysMoveWindow
                                )
import XMonad.Actions.CopyWindow ( copy
                                 , copyToAll
                                 , wsContainingCopies
                                 , killAllOtherCopies
                                 )
import XMonad.Actions.GridSelect ( goToSelected
                                 , bringSelected
                                 , gridselectWorkspace'
                                 )
import XMonad.Actions.RepeatAction (rememberActions)
import XMonad.Actions.EasyMotion (selectWindow)
import XMonad.Actions.Navigation2D ( windowGo
                                   , windowSwap
                                   )
import XMonad.Actions.DynamicProjects ( renameProjectPrompt
                                      , switchProjectPrompt
                                      , shiftToProjectPrompt
                                      , changeProjectDirPrompt
                                      )

import XMonad.Actions.PerWorkspaceKeys (bindOn)

import XMonad.Hooks.ManageDocks ( Direction2D ( U
                                              , D
                                              , R
                                              , L
                                              )
                                , ToggleStruts (ToggleStruts)
                                )

import XMonad.Layout.Gaps ( GapMessage( IncGap
                                      , DecGap
                                      , ModifyGaps
                                      )
                          )
import XMonad.Layout.Hidden ( hideWindow
                            , popOldestHiddenWindow
                            )
import XMonad.Layout.Reflect (REFLECTX (REFLECTX))
import XMonad.Layout.Spacing ( incScreenWindowSpacing
                             , decScreenWindowSpacing
                             )
import XMonad.Layout.SubLayouts ( GroupMsg( UnMerge
                                          , MergeAll
                                          , UnMergeAll
                                          )
                                , onGroup
                                , mergeDir
                                , pushGroup
                                )
import XMonad.Layout.ResizableTile ( MirrorResize ( MirrorShrink
                                                  , MirrorExpand
                                                  )
                                   )
import XMonad.Layout.MultiToggle (Toggle (Toggle))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (NOBORDERS))

import XMonad.Prompt (XPConfig ())
import XMonad.Prompt.Man (manPrompt)
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Unicode (mkUnicodePrompt)
import XMonad.Prompt.ConfirmPrompt (confirmPrompt)

import XMonad.Util.PureX ( view
                         , shift
                         , defile
                         , handlingRefresh
                         )
import XMonad.Util.XUtils ( WindowRect (CustomRect)
                          , WindowConfig ( WindowConfig
                                         , winBg
                                         , winFg
                                         , winFont
                                         , winRect
                                         )
                          )
import XMonad.Util.Ungrab (unGrab)
import XMonad.Util.XSelection (getSelection)
import XMonad.Util.ActionCycle (cycleAction)
import XMonad.Util.NamedScratchpad (namedScratchpadAction)

import App.Alias ( (+++)
                 , bar
                 , term
                 , mail
                 , htop
                 , files
                 , music
                 , inTerm
                 , binDir
                 , browser
                 , dataHome
                 , logToFile
                 , spawnList
                 , browserAlt
                 , andExecute
                 , inImageViewer
                 )
import App.Projects (spaces)
import App.Scratchpad ( fmSP
                      , ghciSP
                      , mailSP
                      , ocamlSP
                      , musicSP
                      , terminalSP
                      , scratchpads
                      )

import Bind.Help (showHelp)

import Config.GridSelect (gsConfig)

import Config.Prompts ( hacksaw
                      , promptTheme
                      , powerPrompt
                      , mediaPrompt
                      , unicodeTheme
                      , volumePrompt
                      , criticalTheme
                      , appImagePrompt
                      , termLaunchPrompt
                      )

import Debug.Debug ( debugNotif
                   , getLayoutDesc
                   )

import Hooks.LayoutHook ( ToggleHints (ToggleHints)
                        , ToggleCentered (ToggleCentered)
                        , emConfig
                        )

import Theme.Theme ( base10
                   , basebg
                   , myBoldFont
                   )

-- End Imports }}}

mappings :: XConfig l -> Map (ButtonMask, KeySym) (X ())
mappings _ = fromList
             . rememberActions (super, xK_Return)
             $ concat [ launchKeys
                      , layoutKeys
                      , mediaCntrl
                      , othersKeys
                      , promptKeys
                      , windowKeys
                      ]

launchKeys, layoutKeys, mediaCntrl, othersKeys, promptKeys, windowKeys :: [((ButtonMask, KeySym), X ())]
launchKeys = [ ((super, xK_BackSpace), unGrab *> spawn (binDir </> "wl" +++ "clip"))
             , ((super, xK_grave), unGrab *> spawn (binDir </> "upload"))
             , ((super, xK_e), spawn term)
             , ((super, xK_n), visualSubmap winConfig . fromList $ [ ((noModMask, xK_d), ("toggle gruvbox dark", spawn $ binDir </> "switch" +++ "d"))
                                                                   , ((noModMask, xK_l), ("toggle gruvbox light", spawn $ binDir </> "switch" +++ "l"))
                                                                   ]
               )
             , ((super, xK_x), powerPrompt promptTheme)
             , ((super, xK_minus), spawn $ binDir </> "wl" +++ "random")
             , ((super, xK_F2), mediaPrompt promptTheme)
             , ((super, xK_F3), spawn $ binDir </> "flash")
             , ((super, xK_F4), spawn $ binDir </> "rs")
             , ((super .|. shft, xK_r), unGrab *> spawn (binDir </> "record"))
             , ((super .|. shft, xK_slash), spawn $ binDir </> "music" +++ "current")
             , ((super .|. ctrl, xK_bracketleft), spawn $ binDir </> "pc")
             , ((alt, xK_b), spawn browser)
             , ((alt, xK_c), stFloat)
             , ((alt, xK_m), spawn . inTerm . andExecute $ mail)
             , ((alt, xK_n), spawn . inTerm . andExecute $ music)
             , ((alt, xK_l), visualSubmap winConfig . fromList $ [ ((noModMask, xK_g), ("launch gimp", spawn "gimp"))
                                                                 , ((noModMask, xK_n), ("launch nicotine+", spawn "nicotine"))
                                                                 , ((noModMask, xK_f), ("launch filezilla", spawn "filezilla"))
                                                                 , ((noModMask, xK_t), ("launch telegram", spawn "telegram-desktop"))
                                                                 , ((noModMask, xK_s), ("launch signal", spawn "signal-desktop-beta"))
                                                                 , ((noModMask, xK_d), ("launch discord", spawn $ binDir </> "other" </> "discocss"))
                                                                 ]
               )
             , ((alt, xK_g), showHelp)
             , ((alt, xK_h), spawn . inTerm . andExecute $ htop)
             , ((alt, xK_f), spawn . inTerm . andExecute $ files)
             , ((alt, xK_p), spawn . inTerm . andExecute $ "pulsemixer")
             , ((alt, xK_s), spawn $ binDir </> "scr" +++ "sel")
             , ((alt, xK_v), spawn "virt-manager")
             , ((alt, xK_w), spawn . inImageViewer . ("-rt" +++) $ dataHome </> "src" </> "wallpapers")
             , ((alt, xK_x), do unGrab
                                spawnList ["xcolor", "|", "tr -d '\n'", "|", "xclip -sel clip -in"]
                                debugNotif "Color copied to clipboard!"
               )
             , ((alt, xK_z), spawn browserAlt)
             , ((ctrl, xK_Escape), spawn $ binDir </> "vol" +++ "toggle_mic")
             , ((ctrl .|. shft, xK_l), unGrab *> spawnList ["xset", "s", "activate", "&&", binDir </> "locker", "&"])
             ]
layoutKeys = [ ((super, xK_space), sendMessage NextLayout)
             , ((super, xK_b), sendMessageAll ToggleStruts *> cycleAction "bar" [ spawnList ["pkill", "-x", "-9", "xmobar"]
                                                                                , spawnList [bar, "&"]
                                                                                ]
               )
             , ((super, xK_f), toggleFullscreen)
             , ((super, xK_i), withFocused (sendMessage . UnMerge) *> windows focusUp)
             , ((super, xK_r), getLayoutDesc >>= \case
                    "tall" -> sendMessage $ Toggle REFLECTX
                    _ -> return ()
               )
             , ((super, xK_s), withFocused toggleFloat)
             , ((super, xK_t), withFocused $ sendMessage . mergeDir id)
             , ((super, xK_comma), getLayoutDesc >>= \case
                    "full" -> return ()
                    _ -> sendMessage $ IncMasterN 1
               )
             , ((super, xK_period), getLayoutDesc >>= \case
                    "full" -> return ()
                    _ -> sendMessage $ IncMasterN $ -1
               )
             , ((super .|. shft, xK_minus), getLayoutDesc >>= \case
                    "full" -> return ()
                    _ -> sendMessage $ ModifyGaps $ resizeGaps $ subtract 3
               )
             , ((super .|. shft, xK_equal), getLayoutDesc >>= \case
                    "full" -> return ()
                    _ -> sendMessage $ ModifyGaps $ resizeGaps $ add 3
               )
             , ((super .|. ctrl, xK_m), cycleAction "tabAll" [ withFocused $ sendMessage . MergeAll
                                                             , withFocused $ sendMessage . UnMergeAll
                                                             ]
               )
             , ((super .|. shft, xK_space), resetLayout *> setAllLayout)
             , ((super .|. ctrl, xK_space), resetLayout)
             , ((super .|. shft, xK_b), sendMessage $ Toggle NOBORDERS)
             , ((super .|. shft, xK_f), unGrab *> selectWindow emConfig >>= (`whenJust` sendMessage . mergeDir id))
             , ((super .|. shft, xK_i), withFocused $ sendMessage . UnMergeAll)
             , ((super .|. shft, xK_m), sendMessage NextLayout)
             , ((super .|. ctrl .|. shft, xK_space), setAllLayout)
             , ((super .|. ctrl, xK_t), sendMessage $ Toggle ToggleHints)
             , ((super .|. ctrl, xK_q), sendMessage $ Toggle ToggleCentered)
             , ((super .|. ctrl, xK_equal), incScreenWindowSpacing 1)
             , ((super .|. ctrl, xK_minus), decScreenWindowSpacing 1)
             , ((super .|. alt, xK_space), refresh)
             , ((super .|. alt, xK_y), toggleCopyToAll)
             ]
mediaCntrl = [ ((noModMask, xF86XK_AudioMute), spawnList [binDir </> "vol", "mute"])
             , ((noModMask, xF86XK_AudioPlay), liftIO . void $ withMPD toggle)
             , ((noModMask, xF86XK_AudioPrev), liftIO . void $ withMPD previous)
             , ((noModMask, xF86XK_AudioNext), liftIO . void $ withMPD next)
             , ((noModMask, xF86XK_AudioStop), liftIO . void $ withMPD stop)
             , ((noModMask, xF86XK_AudioLowerVolume), spawnList [binDir </> "vol", "-"])
             , ((noModMask, xF86XK_AudioRaiseVolume), spawnList [binDir </> "vol", "+"])
             , ((noModMask, xF86XK_MonBrightnessUp), spawnList ["xbacklight", "-inc", "10"])
             , ((noModMask, xF86XK_MonBrightnessDown), spawnList ["xbacklight", "-dec", "10"])
             , ((super, xK_KP_Divide), cycleAction "single" [ liftIO . void . withMPD $ single True *> debugNotif "single on!"
                                                            , liftIO . void . withMPD $ single False *> debugNotif "single off!"
                                                            ]
               )
             , ((super, xK_KP_Multiply), cycleAction "repeat" [ liftIO . void . withMPD $ repeat True *> debugNotif "repeat on!"
                                                              , liftIO . void . withMPD $ repeat False *> debugNotif "repeat off!"
                                                              ]
               )
             , ((super .|. ctrl, xK_b), liftIO . void . withMPD $ seekCur False (-5))
             , ((super .|. ctrl, xK_f), liftIO . void . withMPD $ seekCur False 5)
             , ((noModMask, xK_Print), unGrab *> spawn (binDir </> "scr" +++ "full"))
             , ((shft, xK_Print), unGrab *> spawn (binDir </> "scr" +++ "show"))
             , ((super .|. alt, xK_o), getSelection >>= \sel -> spawnList ["mpv", sel])
             ]
othersKeys = [ ((noModMask, xF86XK_Tools), namedScratchpadAction scratchpads musicSP)
             , ((super, xK_g), namedScratchpadAction scratchpads ghciSP)
             , ((super, xK_m), namedScratchpadAction scratchpads musicSP)
             , ((super, xK_v), namedScratchpadAction scratchpads terminalSP)
             , ((super, xK_z), namedScratchpadAction scratchpads fmSP)
             , ((super .|. ctrl, xK_c), namedScratchpadAction scratchpads ocamlSP)
             , ((super .|. ctrl, xK_m), namedScratchpadAction scratchpads mailSP)
             ]
promptKeys = [ ((super, xK_o), visualSubmap winConfig $ searchList $ promptSearch promptTheme)
             , ((super, xK_p), visualSubmap winConfig . promptList $ promptTheme)
             , ((super, xK_u), unGrab *> mkUnicodePrompt "xclip" ["-sel", "clip", "-in"] "/usr/share/unicode/UnicodeData.txt" unicodeTheme)
             , ((alt .|. shft, xK_c), termLaunchPrompt promptTheme)
             , ((noModMask, xF86XK_Launch1), termLaunchPrompt promptTheme)
             , ((super .|. shft, xK_q), unGrab *> confirmPrompt criticalTheme "Quit?" quitXMonad)
             , ((super .|. shft, xK_w), unGrab *> confirmPrompt criticalTheme "Kill All?" killAll)
             , ((super .|. alt, xK_Return), unGrab *> shellPrompt promptTheme)
             ]
windowKeys = [ ((super, xK_a), cycleAction "hide" [withFocused hideWindow, popOldestHiddenWindow])
             , ((super, xK_c), windows focusDown)
             , ((super, xK_d), gridselectWorkspace' (gsConfig 100) (\ws -> defile $ shift ws <> view ws))
             , ((super, xK_q), do void $ liftIO getDirectories >>= \xDirs -> recompile xDirs False
                                  restart "xmonad" True
               )
             , ((super, xK_w), withFocused killWindow)
             , ((super, xK_y), withFocused $ windows . sink)
             , ((super, xK_bracketleft), unGrab *> goToSelected (gsConfig 250))
             , ((super, xK_bracketright), unGrab *> bringSelected (gsConfig 250))
             , ((super, xK_backslash), inputMode . toList $ resizeMode)
             , ((super, xK_semicolon), bindOn [("Tabs", windows focusUp), ("", onGroup focusUp')])
             , ((super, xK_apostrophe), bindOn [("Tabs", windows focusDown), ("", onGroup focusDown')])
             , ((super .|. shft, xK_c), windows focusUp)
             , ((super .|. shft, xK_d), windows focusMaster)
             , ((super .|. shft, xK_k), unGrab *> selectWindow emConfig >>= (`whenJust` killWindow))
             , ((super .|. ctrl, xK_w), withFocused $ \w -> spawnList ["xkill", "-id", show w])
             , ((super .|. ctrl .|. shft, xK_w), withUnfocused killWindow)
             , ((ctrl, xK_m), windows swapMaster)
             , ((super .|. alt, xK_l), withWindowSet $ logToFile . show)
             , ((ctrl, xK_semicolon), bindOn [("Tabs", windows swapUp), ("", windows swapUp)])
             , ((ctrl, xK_apostrophe), bindOn [("Tabs", windows swapDown), ("", windows swapDown)])
             ]
          <> [ ((super, k), windowGo d False) | (k, d) <- zip dirKeys dirs ]
          <> [ ((ctrl, k), windowSwap d False) | (k, d) <- zip dirKeys dirs ]
          <> [ ((super .|. alt, k), windows $ copy i) | (k,i) <- zip keyPadKeys spaces ]
          <> [ ((super .|. alt, k), windows $ copy i) | (k,i) <- zip workspaceKeys spaces ]
          <> [ ((super .|. ctrl, k), sendMessage $ pushGroup d) | (k, d) <- zip dirKeys dirs ]
          <> [ ((super, k), withFocused $ keysMoveWindow d) | (k, d) <- zip arrowKeys moveDimensions ]
          <> [ ((super .|. ctrl .|. shft, k), sendMessage $ DecGap 5 d) | (k, d) <- zip arrowKeys dirs ]
          <> [ ((super .|. ctrl, k), sendMessage $ IncGap 5 d) | (k, d) <- zip (reverse arrowKeys) dirs ]
          <> [ ((super .|. m, k), defile f) | (i, k) <- zip spaces keyPadKeys , (m, f) <- [(noModMask, view i), (shft, shift i <> view i)] ]
          <> [ ((super .|. m, k), defile f) | (i, k) <- zip spaces workspaceKeys, (m, f) <- [(noModMask, view i), (shft, shift i <> view i)] ]
          <> [ ((super .|. ctrl, k), spawnList ["mpc", "-q", "seek", i <> "%"])
                | (k,i) <- zip [xK_0..xK_9] $ show <$> ([0, 10, 20, 30, 40, 50, 60, 70, 80, 90] :: [Int])
             ]

rust, ebay, urban, reddit, github, archWiki, azLyrics, youtubeMusic :: SearchEngine
rust = searchEngine "rust" "https://doc.rust-lang.org/std/index.html?search="
ebay = searchEngine "ebay" "https://www.ebay.com/sch/i.html?_nkw="
urban = searchEngine "urban" "https://www.urbandictionary.com/define.php?term="
reddit = searchEngine "reddit" "https://www.reddit.com/search/?q="
github = searchEngine "github" "https://github.com/search?q="
archWiki = searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
azLyrics = searchEngine "azlyrics" "https://search.azlyrics.com/search.php?q="
youtubeMusic = searchEngine "youtubemusic" "https://music.youtube.com/search?q="

searchList :: (SearchEngine -> a) -> Map (ButtonMask, KeySym) (String, a)
searchList m = fromList [ ((noModMask, xK_a), ("archwiki", m archWiki))
                        , ((noModMask, xK_d), ("duckduckgo", m duckduckgo))
                        , ((noModMask, xK_e), ("ebay", m ebay))
                        , ((noModMask, xK_g), ("github", m github))
                        , ((noModMask, xK_h), ("hoogle", m hoogle))
                        , ((noModMask, xK_i), ("imdb", m imdb))
                        , ((noModMask, xK_l), ("azlyrics", m azLyrics))
                        , ((noModMask, xK_m), ("azlyrics", m youtubeMusic))
                        , ((noModMask, xK_r), ("reddit", m reddit))
                        , ((noModMask, xK_s), ("stackage", m stackage))
                        , ((noModMask, xK_t), ("thesaurus", m thesaurus))
                        , ((noModMask, xK_v), ("vocabulary", m vocabulary))
                        , ((noModMask, xK_u), ("urban", m urban))
                        , ((noModMask, xK_w), ("wikipedia", m wikipedia))
                        , ((noModMask, xK_y), ("youtube", m youtube))
                        , ((noModMask, xK_z), ("amazon", m amazon))
                        , ((shft, xK_r), ("rust docs", m rust))
                        , ((shft, xK_w), ("wayback machine", m wayback))
                        ]

alt, shft, ctrl, super :: ButtonMask
alt = mod1Mask
shft = shiftMask
super = mod4Mask
ctrl = controlMask

moveDimensions :: [ChangeDim]
moveDimensions = [(-(50 :: Int), 0), (0, 50), (0, -(50 :: Int)), (50, 0)]

dirs :: [Direction2D]
dirs = [L, D, U, R]

dirKeys :: [KeySym]
dirKeys = [xK_h, xK_j, xK_k, xK_l]

arrowKeys :: [KeySym]
arrowKeys = [xK_Left, xK_Down, xK_Up, xK_Right]

quitXMonad :: X ()
quitXMonad = sendMessageAll ReleaseResources *> liftIO exitSuccess

sendMessageAll :: Message a => a -> X ()
sendMessageAll m = handlingRefresh $ broadcastMessage m

resizeGaps :: (Functor f, Bifunctor p) => (c -> d) -> f (p b c) -> f (p b d)
resizeGaps = (<$>) . second

add :: Num p => p -> p -> p
add = (+)

keyPadKeys, workspaceKeys :: [KeySym]
keyPadKeys = [ xK_KP_End
             , xK_KP_Down
             , xK_KP_Page_Down
             , xK_KP_Left
             , xK_KP_Begin
             , xK_KP_Right
             , xK_KP_Home
             , xK_KP_Up
             , xK_KP_Page_Up
             , xK_KP_Insert
             ]
workspaceKeys = [xK_1..xK_9] <> [xK_0]

promptList :: XPConfig -> Map (ButtonMask, KeySym) (String, X ())
promptList theme = fromList [ ((noModMask, xK_m), ("man pages", manPrompt theme))
                            , ((noModMask, xK_v), ("volume control", volumePrompt theme))
                            , ((noModMask, xK_a), ("appimage launcher", appImagePrompt theme))
                            , ((noModMask, xK_r), ("rename a project", renameProjectPrompt theme))
                            , ((noModMask, xK_c), ("project switcher", switchProjectPrompt theme))
                            , ((noModMask, xK_s), ("project shifter", shiftToProjectPrompt theme))
                            , ((noModMask, xK_d), ("change directory", changeProjectDirPrompt theme))
                            ]

resizeMode :: Map (ButtonMask, KeySym) (String, X ())
resizeMode = fromList [ ((noModMask, xK_h), ("resize leftwards", getLayoutDesc >>= \case
                             "tall" -> sendMessage Shrink
                             _ -> return ()
                      ))
                      , ((noModMask, xK_j), ("resize downwards", getLayoutDesc >>= \case
                             "tall" -> sendMessage MirrorShrink
                             _ -> return ()
                       ))
                      , ((noModMask, xK_k), ("resize upwards", getLayoutDesc >>= \case
                             "tall" -> sendMessage MirrorExpand
                             _ -> return ()
                       ))
                      , ((noModMask, xK_l), ("resize rightwards", getLayoutDesc >>= \case
                             "tall" -> sendMessage Expand
                             _ -> return ()
                       ))
                      , ((noModMask, xK_Tab), ("shift focus", do windows focusDown
                                                                 visualSubmap winConfig resizeMode
                       ))
                      , ((noModMask, xK_Up), ("move floating upwards", withFocused $ keysMoveWindow (0, -(50 :: Int))))
                      , ((noModMask, xK_Down), ("move floating downwards", withFocused $ keysMoveWindow (0, 50)))
                      , ((noModMask, xK_Right), ("move floating rightwards", withFocused $ keysMoveWindow (50, 0)))
                      , ((noModMask, xK_Left), ("move floating leftwards", withFocused $ keysMoveWindow (-(50 :: Int), 0)))
                      ]

toggleFloat :: Window -> X ()
toggleFloat w = windows $ \s ->
    if member w $ floating s
       then sink w s
       else float w (RationalRect (1/6) (1/6) (2/3) (2/3)) s

toggleFullscreen :: X ()
toggleFullscreen = withWindowSet $ \ws ->
    withFocused $ \w -> do let fullRect = RationalRect 0 0 1 1
                           let isFullFloat = w `lookup` floating ws == Just fullRect
                           windows $ if isFullFloat then sink w else float w fullRect

toggleCopyToAll :: X ()
toggleCopyToAll = wsContainingCopies >>= \case
        [] -> windows copyToAll
        _ -> killAllOtherCopies

resetLayout :: X ()
resetLayout = asks (layoutHook . config) >>= setLayout

setAllLayout :: X ()
setAllLayout = do ss@StackSet { current = c@Screen {workspace = ws}
                              , visible = sVisible
                              , hidden = sHidden
                              } <- gets windowset
                  void $ handleMessage (layout ws) $ SomeMessage ReleaseResources
                  ll <- gets $ layout . workspace . current . windowset
                  windows . const $ ss { current = c {workspace = ws {layout = ll}}
                                       , visible = setSL ll sVisible
                                       , hidden = setHL ll sHidden
                                       } where
                                           setHL l = ((\w -> w {layout = l}) <$>)
                                           setSL l = ((\w -> w {workspace = (workspace w){layout = l}}) <$>)

inputMode :: [((ButtonMask, KeySym), (String, X ()))] -> X ()
inputMode xs = visualSubmap winConfig . fromList $ modeMap where
    modeMap = [(b, (f, x *> (visualSubmap winConfig . fromList) modeMap)) | (b, (f, x)) <- xs] <> [((noModMask, xK_Escape), ("exit", return ()))]

winConfig :: WindowConfig
winConfig = WindowConfig { winBg = base10
                         , winFg = basebg
                         , winFont = myBoldFont
                         , winRect = CustomRect Rectangle { rect_y = -40
                                                          , rect_x = 1600
                                                          , rect_width = 350
                                                          , rect_height = 430
                                                          }
                         }

stFloat :: X ()
stFloat = hacksaw >>= \[w, h, x, y] -> do
    let [g1, g2, g3, g4] = show . truncate <$> [w/8, h/18, x, y]
    spawnList [term, "-g", concat [g1, "x", g2, "+", g3, "+", g4], "-c", "st-float"]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
