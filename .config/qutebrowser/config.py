#! /usr/bin/env python3

import os
import subprocess

from qutebrowser.api import interceptor

def filter_yt(info: interceptor.Request):
    url = info.request_url
    if (
        url.host() == "www.youtube.com"
        and url.path() == "/get_video_info"
        and "&adformat=" in url.query()
    ):
        info.block()

interceptor.register(filter_yt)

def read_xresources(prefix):
    props = {}
    x = subprocess.run(["xrdb", "-query"], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split("\n")
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(":\t")
        props[prop] = value
    return props

xprop = read_xresources("*")

basebg = xprop["*.background"]
basefg = xprop["*.foreground"]
base00 = xprop["*.color0"]
base08 = xprop["*.color8"]
base01 = xprop["*.color1"]
base09 = xprop["*.color9"]
base02 = xprop["*.color2"]
base10 = xprop["*.color10"]
base03 = xprop["*.color3"]
base11 = xprop["*.color11"]
base04 = xprop["*.color4"]
base12 = xprop["*.color12"]
base05 = xprop["*.color5"]
base13 = xprop["*.color13"]
base06 = xprop["*.color6"]
base14 = xprop["*.color14"]
base07 = xprop["*.color7"]
base15 = xprop["*.color15"]
base16 = xprop["*.color16"]
base17 = xprop["*.color17"]
base18 = xprop["*.color18"]

config.load_autoconfig(False)

c.scrolling.bar = "never"
c.scrolling.smooth = True

c.search.ignore_case = "always"

c.messages.timeout = 5000

c.url.start_pages = "https://duckduckgo.com"
c.url.default_page = "https://duckduckgo.com"

c.content.headers.user_agent = "Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0"

# config.set("config-cycle content.user_stylesheets ~/.config/qutebrowser/styles/discord.css" "")
# c.colors.webpage.darkmode.enabled = True
# c.colors.webpage.darkmode.algorithm = "brightness-rgb"
# c.colors.webpage.darkmode.policy.images = "smart"
# c.colors.webpage.darkmode.contrast = 0

c.content.pdfjs = True
c.backend = "webengine"
c.content.plugins = True
c.input.media_keys = True
c.hints.uppercase = False
c.content.autoplay = False
c.auto_save.session = True
c.content.mouse_lock = True
c.auto_save.interval = 3600
c.content.local_storage = True
c.content.cookies.accept = "all"
c.content.desktop_capture = True
c.content.javascript.enabled = True
c.input.escape_quits_reporter = True
c.input.insert_mode.auto_leave = True
c.input.insert_mode.auto_load = False
c.content.media.audio_capture = True
c.content.media.video_capture = True
c.content.media.audio_video_capture = True
c.content.notifications.enabled = True
c.content.persistent_storage = True
c.colors.webpage.darkmode.policy.page = "smart"
c.window.title_format = "{current_title} - QuteBrowser"
c.content.javascript.can_access_clipboard = True
c.content.javascript.can_open_tabs_automatically = True

config.bind("<g, s>", "config-source")
config.bind("<t, g>", "toggle-selection")
config.bind("<g, h>", "back")
config.bind("<g, l>", "forward")
config.bind("H", "tab-prev")
config.bind("L", "tab-next")
config.bind("<Ctrl-e>", "open-editor", "insert")

config.set('fonts.web.size.default', 16, '*://github.com')
config.set('fonts.web.size.default', 16, 'wiki.archlinux.org')
config.set('fonts.web.size.default', 16, '*://*.wikipedia.org')
config.set('fonts.web.size.default', 16, '*://*.wiktionary.org')

c.editor.command = [ os.environ["TERMINAL"]
                   , "-e"
                   , os.environ["EDITOR"]
                   , "-f"
                   , "{file}"
                   , "-c"
                   , "normal {line}G{column0}1"
                   ]

c.confirm_quit = ["downloads"]

c.fonts.default_family = "15px FantasqueSans Mono"
c.fonts.keyhint = "15px FantasqueSans Mono"
c.fonts.completion.category = "15px FantasqueSans Mono"
c.fonts.completion.entry = "15px FantasqueSans Mono"
c.fonts.tabs.selected = "14px FantasqueSans Mono"
c.fonts.tabs.unselected = "14px FantasqueSans Mono"
c.fonts.statusbar = "15px FantasqueSans Mono"
c.fonts.downloads = "15px FantasqueSans Mono"
c.fonts.hints = "12px FantasqueSans Mono"
c.fonts.prompts = "15px FantasqueSans Mono"
c.fonts.debug_console = "15px FantasqueSans Mono"
c.fonts.downloads = "15px FantasqueSans Mono"
c.fonts.messages.error = "15px FantasqueSans Mono"
c.fonts.messages.info = "15px FantasqueSans Mono"
c.fonts.messages.warning = "15px FantasqueSans Mono"
c.fonts.web.family.fixed = "FantasqueSans Mono"
c.fonts.web.family.fantasy = "FantasqueSans Mono"
c.fonts.web.family.sans_serif = "FantasqueSans Mono"
c.fonts.web.family.serif = "FantasqueSans Mono"
c.fonts.web.family.standard = "FantasqueSans Mono"
c.fonts.web.family.cursive = "FantasqueSans Mono"
c.fonts.web.size.default = 16
c.fonts.web.size.default_fixed = 16
c.fonts.web.size.minimum = 6
c.fonts.web.size.minimum_logical = 6

c.colors.completion.fg = basefg
c.colors.completion.odd.bg = basebg
c.colors.completion.even.bg = basebg
c.colors.completion.category.fg = base03
c.colors.completion.category.bg = basebg
c.colors.completion.category.border.top = basebg
c.colors.completion.category.border.bottom = basebg
c.colors.completion.item.selected.fg = basebg
c.colors.completion.item.selected.bg = base05
c.colors.completion.item.selected.border.top = basebg
c.colors.completion.item.selected.border.bottom = basebg
c.colors.completion.item.selected.match.fg = basebg
c.colors.completion.match.fg = base09
c.colors.completion.scrollbar.fg = base05
c.colors.completion.scrollbar.bg = basebg

c.completion.show = "auto"
c.completion.shrink = True
c.completion.height = "50%"
c.completion.scrollbar.width = 0
c.completion.scrollbar.padding = 0
c.completion.use_best_match = True
c.completion.cmd_history_max_items = -1
c.completion.timestamp_format = "%H:%M:%S - %d-%m-%Y"

c.colors.contextmenu.menu.bg = basebg
c.colors.contextmenu.menu.fg = base05
c.colors.contextmenu.disabled.bg = basebg
c.colors.contextmenu.disabled.fg = base07
c.colors.contextmenu.selected.bg = base05
c.colors.contextmenu.selected.fg = basebg

c.downloads.remove_finished = 0
c.colors.downloads.bar.bg = basebg
c.colors.downloads.stop.fg = basebg
c.colors.downloads.stop.bg = base02
c.colors.downloads.start.fg = basebg
c.colors.downloads.start.bg = base02
c.colors.downloads.error.fg = base08
c.colors.downloads.system.bg = 'none'
c.colors.downloads.system.fg = 'none'
c.downloads.location.directory = "~/dl"

c.hints.radius = 0
c.keyhint.radius = 0
c.colors.hints.fg = basefg
c.colors.hints.bg = basebg
c.colors.keyhint.bg = basebg
c.colors.keyhint.fg = base11
c.colors.hints.match.fg = basefg

c.hints.auto_follow = "unique-match"
c.hints.border = "1px solid " + base14
c.hints.chars = "abcdefghijklmnoqrstuvwxyz"

c.statusbar.show = "in-mode"
c.statusbar.position = "top"

c.colors.statusbar.caret.bg = base05
c.colors.statusbar.caret.fg = basebg
c.colors.statusbar.insert.bg = basebg
c.colors.statusbar.insert.fg = base01
c.colors.statusbar.normal.bg = basebg
c.colors.statusbar.normal.fg = basefg
c.colors.statusbar.command.bg = basebg
c.colors.statusbar.command.fg = basefg
c.colors.statusbar.private.bg = basebg
c.colors.statusbar.private.fg = base04
c.colors.statusbar.progress.bg = base01
c.colors.statusbar.url.warn.fg = base01
c.colors.statusbar.passthrough.bg = basebg
c.colors.statusbar.passthrough.fg = base11
c.colors.statusbar.caret.selection.bg = base04
c.colors.statusbar.caret.selection.fg = basebg
c.colors.statusbar.command.private.bg = base02
c.colors.statusbar.command.private.fg = basebg
c.colors.statusbar.url.success.https.fg = base02
# c.colors.statusbar.progress.fg = basebg

c.colors.statusbar.caret.bg = basebg
c.colors.statusbar.caret.fg = basefg
c.statusbar.padding = {"top": 5, "bottom": 5, "left": 5, "right": 5}
c.statusbar.widgets = ["tabs", "progress"]

c.tabs.show = "multiple"
c.tabs.position = "bottom"
c.tabs.pinned.shrink = True
c.tabs.indicator.width = 0
c.tabs.favicons.show = "pinned"
c.tabs.favicons.scale = 1
c.tabs.title.format = "{audio}{current_title}"
c.tabs.title.format_pinned = ""
c.tabs.select_on_remove = "last-used"
c.tabs.padding = {"top": 5, "bottom": 5, "left": 5, "right": 5}
c.tabs.indicator.padding = {"top": 2, "bottom": 2, "left": 2, "right": 2}

c.colors.tabs.bar.bg = basebg
c.colors.tabs.odd.fg = basefg
c.colors.tabs.odd.bg = basebg
c.colors.tabs.even.fg = basefg
c.colors.tabs.even.bg = basebg
c.colors.tabs.pinned.odd.bg = base06
c.colors.tabs.pinned.odd.fg = basefg
c.colors.tabs.indicator.stop = base06
c.colors.tabs.pinned.even.bg = base06
c.colors.tabs.pinned.even.fg = basefg
c.colors.tabs.indicator.start = base04
c.colors.tabs.indicator.error = base01
c.colors.tabs.selected.odd.fg = basebg
c.colors.tabs.selected.odd.bg = base13
c.colors.tabs.selected.even.fg = basebg
c.colors.tabs.selected.even.bg = base13
c.colors.tabs.indicator.system = "none"
c.colors.tabs.pinned.selected.odd.bg = base13
c.colors.tabs.pinned.selected.odd.fg = basebg
c.colors.tabs.pinned.selected.even.bg = base13
c.colors.tabs.pinned.selected.even.fg = basebg

c.prompt.radius = 0
c.colors.prompts.bg = basebg
c.colors.prompts.border = basebg
c.colors.prompts.fg = basefg
c.colors.prompts.selected.bg = base07

c.colors.messages.info.bg = basebg
c.colors.messages.info.fg = basefg
c.colors.messages.info.border = basebg
c.colors.messages.error.bg = base01
c.colors.messages.error.fg = basebg
c.colors.messages.error.border = base01
c.colors.messages.warning.bg = base03
c.colors.messages.warning.fg = basebg
c.colors.messages.warning.border = base03
c.messages.timeout = 900
