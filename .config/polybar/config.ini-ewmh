[c]
basebg = ${env:basebg}
basefg = ${env:basefg}
base00 = ${env:base00}
base08 = ${env:base08}
base01 = ${env:base01}
base09 = ${env:base09}
base02 = ${env:base02}
base10 = ${env:base10}
base03 = ${env:base03}
base11 = ${env:base11}
base04 = ${env:base04}
base12 = ${env:base12}
base05 = ${env:base05}
base13 = ${env:base13}
base06 = ${env:base06}
base14 = ${env:base14}
base07 = ${env:base07}
base15 = ${env:base15}

[module/ewmh]
type = internal/xworkspaces
format = <label-state>
pin-workspaces = false
enable-click = true
enable-scroll = false
reverse-scroll = false

label-active = ${env:focus}
; label-active-underline = ${c.base16}
label-active-background = ${c.basebg}
label-active-foreground = ${c.basefg}
label-active-padding = 1
label-active-font = 1

label-occupied = ${env:active}
label-occupied-background = ${c.basebg}
label-occupied-foreground = ${c.basefg}
label-occupied-padding = 1
label-occupied-font = 1

label-urgent = ${env:urgent}
label-urgent-background = ${c.basebg}
label-urgent-foreground = ${c.base10}
label-urgent-padding = 1
label-urgent-font = 1

label-empty =
label-empty-background = ${c.basebg}
label-empty-foreground = ${c.basefg}
label-empty-padding = 0
label-empty-font = 0

format-foreground = ${c.basefg}
format-background = ${c.basebg}

[module/layout]
type = custom/script
exec = tail -F "$XMONAD_CACHE_HOME/layout"
execif = [ -p "$XMONAD_CACHE_HOME/layout" ]
tail = true

label = "%output%"
label-background = ${c.basebg}
label-foreground = ${c.base05}
label-padding = 1
label-font = 6

[module/wincount]
type = custom/script
exec = tail -F "$XMONAD_CACHE_HOME/windows"
execif = [ -p "$XMONAD_CACHE_HOME/windows" ]
tail = true

label = "%output%"
label-background = ${c.basebg}
label-foreground = ${c.basefg}
label-padding = 1
label-font = 6

[module/title]
type = internal/xwindow

label = "%title%"
label-background = ${c.basebg}
label-foreground = ${c.base08}
label-padding = 1
label-maxlen = 95
label-font = 1

label-empty = ""
label-empty-background = ${c.basebg}
label-empty-foreground = ${c.base08}
label-empty-padding = 1
label-empty-font = 1

[module/pulseaudio]
type = internal/pulseaudio

format-volume = " <label-volume>"
format-volume-background = ${c.basebg}
format-volume-foreground = ${c.base06}
format-volume-padding = 1
format-volume-font = 2

label-volume = "%percentage%%"
label-volume-background = ${c.basebg}
label-volume-foreground = ${c.basefg}
label-volume-padding = 0
label-volume-font = 1

format-muted = " <label-muted>"
format-muted-background = ${c.basebg}
format-muted-foreground = ${c.base01}
format-muted-padding = 1
format-muted-font = 2

label-muted = "0%"
label-muted-background = ${c.basebg}
label-muted-foreground = ${c.basefg}
label-muted-padding = 0
label-muted-font = 1

[module/backlight]
type = internal/backlight
card = intel_backlight
enable-scroll = true

format = "<ramp> <label>"
format-background = ${c.basebg}
format-foreground = ${c.basefg}
format-padding = 1
format-font = 1

label = "%percentage%%"
label-background = ${c.basebg}
label-foreground = ${c.basefg}
label-padding = 0
label-font = 1

ramp-0 = ""
ramp-0-background = ${c.basebg}
ramp-0-foreground = ${c.base11}
ramp-0-padding = 0
ramp-0-font = 2

ramp-1 = ""
ramp-1-foreground = ${c.basebg}
ramp-1-background = ${c.base11}
ramp-1-padding = 0
ramp-1-font = 2

ramp-2 = ""
ramp-2-background = ${c.basebg}
ramp-2-foreground = ${c.base11}
ramp-2-padding = 0
ramp-2-font = 2

ramp-3 = ""
ramp-3-background = ${c.basebg}
ramp-3-foreground = ${c.base11}
ramp-3-padding = 0
ramp-3-font = 2

ramp-4 = ""
ramp-4-background = ${c.basebg}
ramp-4-foreground = ${c.base11}
ramp-4-padding = 0
ramp-4-font = 2

ramp-5 = ""
ramp-5-background = ${c.basebg}
ramp-5-foreground = ${c.base11}
ramp-5-padding = 0
ramp-5-font = 2

ramp-6 = ""
ramp-6-background = ${c.basebg}
ramp-6-foreground = ${c.base11}
ramp-6-padding = 0
ramp-6-font = 2

[module/xkeyboard]
type = internal/xkeyboard
; blacklist-0 = num lock
; blacklist-1 = caps lock

; format = " <label-layout>"
format = " <label-layout>"
format-background = ${c.basebg}
format-foreground = ${c.base13}
format-padding = 1
format-font = 2

label-layout = "%icon%"
label-indicator = "%icon%"
label-layout-background = ${c.basebg}
label-layout-foreground = ${c.basefg}
label-layout-padding = 0
label-layout-font = 1

layout-icon-0 = us;US
layout-icon-1 = bg;BG
layout-icon-2 = gr;GR
layout-icon-3 = fr;FR

indicator-icon-0 = caps lock; וּ
indicator-icon-2 = num lock; 

label-indicator-on-capslock = וּ
label-indicator-off-capslock =
label-indicator-on-numlock = 
label-indicator-off-numlock =

[module/network]
type = internal/network
interface = enp7s0f1
interval = 5

format-connected = "<label-connected>"
format-connected-background = ${c.basebg}
format-connected-foreground = ${c.base12}
format-connected-padding = 1
format-connected-font = 2

label-connected = "connected"
label-connected-background = ${c.basebg}
label-connected-foreground = ${c.basefg}
label-connected-padding = 1
label-connected-font = 1

format-disconnected = "<label-disconnected>"
format-disconnected-background = ${c.basebg}
format-disconnected-foreground = ${c.base01}
format-disconnected-padding = 1
format-disconnected-font = 2

label-disconnected = "%ifname%"
label-disconnected-background = ${c.basebg}
label-disconnected-foreground = ${c.basefg}
label-disconnected-padding = 1
label-disconnected-font = 1

[module/wlan]
type = internal/network
interface = wlan0
interval = 5

format-connected = " <label-connected>"
format-connected-background = ${c.basebg}
format-connected-foreground = ${c.base12}
format-connected-padding = 1
format-connected-font = 2

label-connected = "%essid%"
label-connected-background = ${c.basebg}
label-connected-foreground = ${c.basefg}
label-connected-padding = 0
label-connected-font = 1

format-disconnected = " <label-disconnected>"
format-disconnected-background = ${c.basebg}
format-disconnected-foreground = ${c.base01}
format-disconnected-padding = 1
format-disconnected-font = 5

label-disconnected = ""
label-disconnected-background = ${c.basebg}
label-disconnected-foreground = ${c.basefg}
label-disconnected-padding = 0
label-disconnected-font = 1

[module/cpu]
type = internal/cpu
interval = 5

click-left = "$TERMINAL" -e htop &

format-prefix = "  "
format-prefix-background = ${c.basebg}
format-prefix-foreground = ${c.base06}
format-prefix-padding = 0
format-prefix-font = 2

label = "%percentage:2%% "
label-background = ${c.basebg}
label-foreground = ${c.basefg}
label-padding = 0
label-font = 1

[module/memory]
type = internal/memory
interval = 5

click-right = "$TERMINAL" -e htop &

format-prefix = ""
format-prefix-background = ${c.basebg}
format-prefix-foreground = ${c.base05}
format-prefix-padding = 1
format-prefix-font = 5

label = "%percentage_used%% "
label-background = ${c.basebg}
label-foreground = ${c.basefg}
label-padding = 0
label-font = 1

[module/battery]
type = internal/battery
battery = BAT1
adapter = ACAD
full-at = 99

ramp-capacity-0 = ""
ramp-capacity-background = ${c.basebg}
ramp-capacity-foreground = ${c.base02}
ramp-capacity-padding = 0
ramp-capacity-font = 2

label-full = "%percentage%% "
label-full-background = ${c.basebg}
label-full-foreground = ${c.basefg}
label-full-padding = 0
label-full-font = 1

format-full-prefix = ""
format-full-prefix-background = ${c.basebg}
format-full-prefix-foreground = ${c.base02}
format-full-prefix-padding = 1
format-full-prefix-font = 2

label-charging = "%percentage%% "
label-charging-background = ${c.basebg}
label-charging-foreground = ${c.basefg}
label-charging-padding = 0
label-charging-font = 1

animation-charging-0 = ""

format-charging = "<animation-charging><label-charging>"

animation-charging-background = ${c.basebg}
animation-charging-foreground = ${c.base02}
animation-charging-padding = 1
animation-charging-font = 2

label-discharging = "%percentage%% "
label-discharging-background = ${c.basebg}
label-discharging-foreground = ${c.basefg}
label-discharging-padding = 0
label-discharging-font = 1

animation-discharging-0 = ""
animation-discharging-1 = ""
animation-discharging-2 = ""
animation-discharging-3 = ""
animation-discharging-4 = ""
animation-discharging-framerate = 1000

format-discharging = "<animation-discharging><label-discharging>"

animation-discharging-background = ${c.basebg}
animation-discharging-foreground = ${c.base02}
animation-discharging-padding = 1
animation-discharging-font = 2

[module/date]
type = internal/date
interval = 5

date = "%a %d.%m.%Y"

format-padding = 0

format-prefix = " "
format-prefix-background = ${c.basebg}
format-prefix-foreground = ${c.base09}
format-prefix-padding = 0
format-prefix-font = 2

label = " %date% "
label-background = ${c.basebg}
label-foreground = ${c.basefg}
label-padding = 0
label-font = 1

[module/time]
type = internal/date
interval = 1

time = "%R"

format-prefix = " "
format-prefix-background = ${c.basebg}
format-prefix-foreground = ${c.base03}
format-prefix-padding = 0
format-prefix-font = 2

label = "%time%"
label-background = ${c.basebg}
label-foreground = ${c.basefg}
label-padding = 1
label-font = 1

[global/wm]
margin-top = 0
margin-bottom = 0

[settings]
screenchange-reload = true
; compositing-background = xor
; compositing-background = screen
; compositing-foreground = source
; compositing-border = over
pseudo-transparency = false

[bar/bar]
width = 100%
height = 30
radius = 0
fixed-center = true
bottom = false

enable-ipc = true

background = ${c.basebg}
foreground = ${c.basefg}

line-size = 0

border-size = 0
border-color = ${c.basebg}

padding-left = 0
padding-right = 0

separator = "|"
separator-background = ${c.basebg}
separator-foreground = ${c.base08}
separator-padding = 0
separator-font = 7

module-margin-left = 0
module-margin-right = 0

font-0 = Exosevka:style=Regular:pixelsize=12;2:antialias=true
font-1 = Exosevka Nerd Font:style=Regular:pixelsize=12;2:antialias=true
font-2 = Material Design Icons:style=Regular:pixelsize=10;3:antialias=true
font-3 = Font Awesome 5 Pro:style=Solid:pixelsize=10;2:antialias=true
font-4 = Font Awesome 5 Pro:style=Solid:pixelsize=10;2:antialias=true
font-5 = Exosevka:style=Bold:pixelsize=12;2:antialias=true
font-6 = Exosevka:style=Regular:pixelsize=16;3:antialias=true

modules-left = ewmh layout wincount title
modules-center =
modules-right = pulseaudio xkeyboard wlan battery date time

tray-position = none
tray-padding = 0

cursor-click = pointer
cursor-scroll = pointer

; vim:ft=dosini
