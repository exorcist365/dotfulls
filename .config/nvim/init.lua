require("fm")
require("autocmd")
require("options")
require("corrections")
require("mappings")
require("plugins")
require("colors")

-- vim:ft=lua
