runtime! archlinux.vim
runtime! macros/matchit.vim

silent! source $XDG_CONFIG_HOME/nvim/vimL/config/colors

syntax enable
syntax on
scriptencoding UTF-8
filetype plugin indent on

set termguicolors
set background=dark

set nocompatible

set guioptions+=mTrL
set expandtab smarttab
set splitbelow splitright
set autoindent smartindent
set nonumber norelativenumber
set nocursorline nocursorcolumn
set wrap wrapscan breakindent linebreak
set tabstop=4 softtabstop=-1 shiftwidth=4
set incsearch ignorecase smartcase hlsearch

set fillchars+=vert:\│,eob:\ ,fold:\#,msgsep:‾

if &term =~ '256color'
    set t_ut=
endif

let &t_8f="\<Esc>[41;2;%lu;%lu;%lum"
let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"

set t_Co=254

set encoding=UTF-8

set emoji
set cmdheight=1

set shortmess+=c

set path=.,**

set clipboard+=unnamedplus

set title
set titlestring=%f

set autochdir

set novisualbell
set noerrorbells

set nobackup
set nowritebackup

set noshowcmd
set noshowmode

set notimeout
set nottimeout

set noequalalways

set noswapfile

set mouse=a
set matchtime=-1

set go+=c

set completeopt=menu,menuone,preview,noselect,noinsert

set scrolloff=10

set foldenable
set foldnestmax=3
set foldmethod=manual

set shell=getenv("SHELL")

set guicursor=

set updatetime=250

set signcolumn=number

set nrformats+=octal

set backspace=indent,eol,start

set undofile
set undodir=~/.local/share/nvim/backups/undo/

set list
set listchars=
set listchars+=tab:#\ ,trail:>,extends:→,precedes:←,nbsp:␣

set statusline=
set statusline+=%!StatusLine('active')

" let g:is_bash='1'
let g:is_posix='1'
let g:rehash256='1'
let g:asmsyntax='nasm'

let $MYVIMRC='~/.config/nvim/old/init.vim'
let mapleader="\<Space>"
let maplocalleader="\<Space>"

if !isdirectory(expand(&undodir))
    call mkdir(expand(&undodir), 'p', 0700)
endif

" vim:ft=vim
