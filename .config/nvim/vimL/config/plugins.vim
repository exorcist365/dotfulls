if ! filereadable(system('printf "%s" "${XDG_CONFIG_HOME:-"$HOME/.config"}/nvim/autoload/plug.vim"'))
    silent !mkdir -pv "${XDG_CONFIG_HOME:-"$HOME/.config"}/nvim/autoload"
    silent !curl -sL "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" -o "${XDG_CONFIG_HOME:-"$HOME/.config"}/nvim/autoload/plug.vim"
    autocmd VimEnter * PlugInstall | source "$MYVIMRC"
endif

call plug#begin('~/.local/share/nvim/plugged')
    Plug 'junegunn/fzf.vim'
    Plug 'hrsh7th/vim-vsnip'
    Plug 'jiangmiao/auto-pairs'
    Plug 'tpope/vim-commentary'

    if has('nvim')
        Plug 'hrsh7th/nvim-compe'
        Plug 'onsails/lspkind-nvim'
        Plug 'neovim/nvim-lspconfig'
        Plug 'lewis6991/impatient.nvim'
        Plug 'norcalli/nvim-colorizer.lua'
    endif

    Plug 'sheerun/vim-polyglot'
        let g:haskell_enable_quantification = 1
        let g:haskell_enable_recursivedo = 1
        let g:haskell_enable_arrowsyntax = 1
        let g:haskell_enable_pattern_synonyms = 1
        let g:haskell_enable_typeroles = 1
        let g:haskell_enable_static_pointers = 1
        let g:haskell_backpack = 1

    Plug 'gruvbox-community/gruvbox'
        let g:gruvbox_bold = 1
        let g:gruvbox_italic = 1
        let g:gruvbox_termcolors = 256
        let g:gruvbox_contrast_dark = 'hard'

    Plug 'MTDL9/vim-log-highlighting', { 'for': 'log' }
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'purescript-contrib/purescript-vim', { 'for': 'purescript' }
call plug#end()

" vim:ft=vim
