silent! source $XDG_CONFIG_HOME/nvim/vimL/config/plugins

colorscheme gruvbox

highlight SignColumn gui=none guibg=#1d2021 guifg=#32302f
highlight LineNr gui=none guibg=#1d2021 guifg=#32302f
highlight CursorLineNr gui=bold guibg=#1d2021 guifg=#b16286
highlight Folded guibg=#1d2021 gui=none
highlight StatusLine guibg=#1d2021 guifg=#1d2021 gui=none
highlight StatusLineNC guibg=#1d2021 guifg=#1d2021 gui=none
highlight Todo gui=italic
highlight Comment gui=italic
highlight Error gui=bold guibg=#1d2021 guifg=#cc241d
highlight Conceal guibg=#1d2021 guifg=#8ec07c gui=bold
highlight VertSplit gui=bold guibg=#1d2021 guifg=#32302f
highlight LspDiagnosticsSignHint gui=none guibg=#1d2021 guifg=#689d6a
highlight LspDiagnosticsSignError gui=none guibg=#1d2021 guifg=#cc241d
highlight LspDiagnosticsSignWarning gui=none guibg=#1d2021 guifg=#d79921
highlight LspDiagnosticsSignInformation gui=none guibg=#1d2021 guifg=#83a598

highlight User0 guibg=#1d2021 guifg=#1d2021 gui=none
highlight User1 guibg=#1d2021 guifg=#ebdbb2 gui=none
highlight User2 guibg=#1d2021 guifg=#b8bb26 gui=bold
" highlight User3 guibg=#1d2021 guifg=#689d6a gui=none
highlight User4 guibg=#1d2021 guifg=#458588 gui=none
highlight User5 guibg=#1d2021 guifg=#cc241d gui=none
highlight User6 guibg=#1d2021 guifg=#b16286 gui=bold
highlight User7 guibg=#1d2021 guifg=#32302f gui=none

highlight link CompeDocumentation NormalFloat

let g:currentmode = {
            \ 'n'  : 'normal',
            \ 'no' : 'n-op',
            \ 'v'  : 'visual',
            \ 'V'  : 'line',
            \ '' : 'block',
            \ 's'  : 'selection',
            \ 'S'  : 'line select',
            \ '' : 'block select',
            \ 'i'  : 'insert',
            \ 'R'  : 'replace',
            \ 'Rv' : 'visual replace',
            \ 'c'  : 'command',
            \ 'cv' : 'vim execute',
            \ 'ce' : 'execute',
            \ 'r'  : 'prompt',
            \ 'rm' : 'more',
            \ 'r?' : 'confirm',
            \ '!'  : 'shell',
            \ 't'  : 'terminal'
            \}

function! RedrawModeColors(mode)
    if a:mode == 'n'
        highlight User3 guifg=#458588 guibg=#1d2021 gui=none
    elseif a:mode == 'i'
        highlight User3 guifg=#689d6a guibg=#1d2021 gui=bold
    elseif a:mode == 'R' || a:mode == 'r' || a:mode == 'Rv'
        highlight User3 guifg=#b16286 guibg=#1d2021 gui=bold
    elseif a:mode == 'v'
        highlight User3 guifg=#98971a guibg=#1d2021 gui=none
    elseif a:mode == ''
        highlight User3 guifg=#d79921 guibg=#1d2021 gui=none
    elseif a:mode == 'c'
        highlight User3 guifg=#d65d0e guibg=#1d2021 gui=none
    elseif a:mode == 't'
        highlight User3 guifg=#cc241d guibg=#1d2021 gui=none
    endif
  redrawstatus
endfunction

function! SetFiletype(filetype)
    if a:filetype == ''
        return '-'
    else
        return a:filetype
    endif
    redrawstatus
endfunction

function! SetModifiedSymbol(modified)
    if a:modified == 1
        highlight User8 guifg=#fabd2f guibg=#1d2021 gui=none
        return '*'
    else
        highlight User8 guibg=none guifg=none gui=none
        return ''
    endif
    redrawstatus
endfunction

function! StatusLine(mode) abort
    let l:line=''

    if a:mode ==# 'active'
        let l:line.='%0*'
        let l:line.='%{RedrawModeColors(mode())}'
        let l:line.='%3*%{g:currentmode[mode()]}'
        let l:line.='%7* |%2* %t'
        let l:line.='%0* %8*%{SetModifiedSymbol(&modified)}'
        let l:line.='%0*%='
        let l:line.='%6*%l'
        let l:line.='%7* |%5* %{SetFiletype(&filetype)}'
        let l:line.='%0*'
    endif

    let l:line.='%*'

    return l:line
endfunction

" vim:ft=vim
