local g = vim.g
local fn = vim.fn
local cmd = vim.cmd
local opt = vim.opt

local function join_paths(...)
    return(table.concat(({...}), "/"))
end

cmd("syntax on")
cmd("syntax enable")
cmd("colorscheme gruvbox")
cmd("filetype plugin indent on")

g["is_posix"] = 1
g["rehash256"] = 1
g["asmsyntax"] = "fasm"

g["mapleader"] = " "
g["maplocalleader"] = ","

g["gruvbox_bold"] = 1
g["gruvbox_italic"] = 1
g["gruvbox_termcolors"] = 256
g["gruvbox_contrast_dark"] = "hard"

g["zig_fmt_autosave"] = 0

local disabled_builtins = ({
    "2html_plugin",
    "getscript",
    "getscriptPlugin",
    "gzip",
    "logipat",
    -- "netrw",
    -- "netrwPlugin",
    -- "netrwSettings",
    -- "netrwFileHandlers",
    "matchit",
    "tar",
    "tarPlugin",
    "rrhelper",
    "spellfile_plugin",
    "vimball",
    "vimballPlugin",
    "zip",
    "zipPlugin"
})

for _, plugin in pairs(disabled_builtins) do
    g["loaded_" .. plugin] = 1
end

opt.switchbuf = "usetab"

opt.virtualedit = "block"

opt.lazyredraw = true

opt.formatoptions:remove("c", "r", "o")

opt.modeline = true
opt.modelines = 1

opt.fileformat = "unix"

opt.background = "dark"
opt.termguicolors = true

opt.compatible = false

opt.smarttab = true
opt.expandtab = true

opt.splitbelow = true
opt.splitright = true

opt.wrap = true
opt.wrapscan = true
opt.autoindent = true
opt.smartindent = true

opt.number = false
opt.relativenumber = false

opt.cursorline = false
opt.cursorcolumn = false

opt.shortmess:append("saATtIc")

opt.linebreak = true
opt.breakindent = true

opt.tabstop = 4
opt.shiftwidth = 4
opt.softtabstop = -1

opt.incsearch = true
opt.smartcase = true
opt.ignorecase = true

opt.hlsearch = false

opt.fillchars:append("vert:│,eob: ,fold:#,msgsep:‾")

opt.encoding = "UTF-8"

opt.emoji = true

opt.cmdheight = 1

opt.clipboard:append("unnamedplus")

opt.title = true
opt.titlestring = "%f"

opt.autochdir = true

opt.visualbell = false
opt.errorbells = false

opt.backup = false
opt.writebackup = false

opt.showcmd = false
opt.showmode = false

opt.timeout = false
opt.ttimeout = false

opt.equalalways = false

opt.swapfile = false

opt.mouse = "a"

opt.matchtime = 1

opt.completeopt = "menuone,preview,noselect,noinsert"

opt.scrolloff = 10

opt.langremap = true
opt.langmap = "АA,аa,БB,бb,ЦC,цc,ДD,дd,ЕE,еe,ФF,фf,ГG,гg,ХH,хh,ИI,иi,ЙJ,йj,КK,кk,ЛL,лl,МM,мm,НN,нn,ОO,оo,ПP,пp,ЯQ,яq,РR,рr,СS,сs,ТT,тt,УU,уu,ЖV,жv,ВW,вw,ѝX,ьx,ЪY,ъy,ЗZ,зz,ч`,11,22,33,44,55,66,77,88,99,00,--,==,ш[,щ],;;,'',ю\\,..,//,Ч~,!!,@@,№#,$$,%%,€^,§&,**,((,)),__,++,Ш{,Щ},::,\"\",Ю|,<<,>>,??"

opt.foldnestmax = 3
opt.foldenable = true
opt.foldmethod = "marker"

opt.shell = vim.env.SHELL

opt.guicursor = ""

opt.ruler = false

opt.updatetime = 250

opt.signcolumn = "auto"

opt.backspace = "indent,eol,start"

opt.undofile = true
opt.undodir = join_paths(fn.stdpath("data"), "undo")

opt.list = true
opt.listchars:append("tab:# ,trail:>,extends:→,precedes:←,nbsp:␣")

-- vim:ft=lua
