local M = ({})
local fn = vim.fn
local opt = vim.opt
local api = vim.api
local fmt = string.format
local concat = table.concat
local cmd = api.nvim_command

M = ({
    colors = ({
        basebg = "#1d2021",
        basefg = "#ebdbb2",
        base00 = "#1d2021",
        base08 = "#928374",
        base01 = "#cc241d",
        base09 = "#fb4934",
        base02 = "#98971a",
        base10 = "#b8bb26",
        base03 = "#d79921",
        base11 = "#fabd2f",
        base04 = "#458588",
        base12 = "#83a598",
        base05 = "#b16286",
        base13 = "#d3869b",
        base06 = "#689d6a",
        base14 = "#8ec07c",
        base07 = "#a89984",
        base15 = "#ebdbb2",
        base16 = "#d65d0e",
        base17 = "#282828",
        base18 = "#32302f"
    })
})

local function hi(group, gui, bg, fg)
    cmd(fmt("highlight %s gui='%s' guibg='%s' guifg='%s'", group, gui, bg, fg))
end

cmd("highlight! link NormalFloat Normal")
cmd("highlight! link FloatBorder Normal")

cmd("highlight! clear Pmenu")
cmd("highlight! clear PmenuSel")
cmd("highlight! clear PmenuSbar")
cmd("highlight! clear PmenuThumb")

cmd("highlight! clear CmpItemMenuDefault")
cmd("highlight! clear CmpItemAbbrDefault")
cmd("highlight! clear CmpItemKindDefault")
cmd("highlight! clear CmpItemAbbrMatchDefault")
cmd("highlight! clear CmpItemAbbrMatchFuzzyDefault")
cmd("highlight! clear CmpItemAbbrDeprecatedDefault")

hi("Pmenu", "none", M.colors.base18, M.colors.basefg)
hi("PmenuSel", "none", M.colors.base12, M.colors.basebg)
hi("PmenuSbar", "none", M.colors.base06, M.colors.basebg)
hi("PmenuThumb", "none", M.colors.base17, M.colors.basebg)

hi("CmpItemMenu", "bold", M.colors.basebg, M.colors.base12)
hi("CmpItemAbbr", "none", M.colors.basebg, M.colors.basefg)
hi("CmpItemKind", "none", M.colors.basebg, M.colors.base11)
hi("CmpItemAbbrMatchDefault", "none", M.colors.basebg, M.colors.basefg)
hi("CmpItemAbbrMatchFuzzyDefault", "none", M.colors.basebg, M.colors.basefg)
hi("CmpItemAbbrDeprecatedDefault", "none", M.colors.basebg, M.colors.basefg)

hi("LineNr", "bold", M.colors.basebg, M.colors.base05)
hi("LineNrAbove", "none", M.colors.basebg, M.colors.base18)
hi("LineNrBelow", "none", M.colors.basebg, M.colors.base18)
hi("SignColumn", "none", M.colors.basebg, M.colors.base18)
hi("CursorLineNr", "bold", M.colors.basebg, M.colors.base05)

hi("Todo", "italic", M.colors.basebg, M.colors.base16)
hi("Comment", "italic", M.colors.basebg, M.colors.base08)

hi("TabLine", "none", M.colors.basebg, M.colors.base18)
hi("TabLineSel", "bold", M.colors.basebg, M.colors.base10)
hi("TabLineFill", "none", M.colors.basebg, M.colors.basebg)

hi("StatusLine", "none", M.colors.basebg, M.colors.basebg)
hi("StatusLineNC", "none", M.colors.basebg, M.colors.basebg)

hi("Error", "bold", M.colors.basebg, M.colors.base01)
hi("Folded", "italic", M.colors.basebg, M.colors.base08)
hi("VertSplit", "bold", M.colors.basebg, M.colors.base18)

hi("DiagnosticSignHint", "none", M.colors.basebg, M.colors.base06)
hi("DiagnosticSignWarn", "none", M.colors.basebg, M.colors.base03)
hi("DiagnosticSignInfo", "none", M.colors.basebg, M.colors.base12)
hi("DiagnosticSignError", "none", M.colors.basebg, M.colors.base01)

hi("User0", "none", M.colors.basebg, M.colors.basebg)
hi("User1", "none", M.colors.basebg, M.colors.basefg)
hi("User2", "bold", M.colors.basebg, M.colors.base10)
hi("User4", "none", M.colors.basebg, M.colors.base04)
hi("User5", "none", M.colors.basebg, M.colors.base01)
hi("User6", "bold", M.colors.basebg, M.colors.base05)
hi("User7", "none", M.colors.basebg, M.colors.base18)
hi("User9", "none", M.colors.basebg, M.colors.base06)

hi("Branch", "none", M.colors.basebg, M.colors.base12)
hi("OtherMode", "none", M.colors.basebg, M.colors.base12)
hi("VisualMode", "none", M.colors.basebg, M.colors.base02)
hi("NormalMode", "none", M.colors.basebg, M.colors.base04)
hi("InsertMode", "bold", M.colors.basebg, M.colors.base06)
hi("ReplaceMode", "bold", M.colors.basebg, M.colors.base05)
hi("CommandMode", "none", M.colors.basebg, M.colors.base16)
hi("TerminalMode", "none", M.colors.basebg, M.colors.base01)
hi("VisualBlockMode", "none", M.colors.basebg, M.colors.base03)

function Statusline()
    local mode_map = ({
        ["n"] = "normal",
        ["no"] = "normal operator pending",
        ["v"] = "visual",
        ["V"] = "visual line",
        [""] = "visual block",
        ["s"] = "select",
        ["S"] = "line select",
        [""] = "block select",
        ["i"] = "insert",
        ["R"] = "replace",
        ["Rv"] = "visual replace",
        ["c"] = "command",
        ["cv"] = "vim execute",
        ["ce"] = "execute",
        ["r"] = "prompt",
        ["rm"] = "more",
        ["r?"] = "confirm",
        ["!"] = "shell",
        ["t"] = "terminal"
    })

    local function get_mode()
        local mode = fn.mode()

        if (mode == nil) then
            return("")
        else
            return(mode_map[mode])
        end
    end

    local function get_branch()
        local branch = fn.system({ "git", "symbolic-ref", "--short", "HEAD" })

        if (#branch > 30) then
            return("")
        else
            return("%7* | %#Branch#" .. branch:sub(0, #branch - 1))
        end
    end

    local function colorize_mode()
        local mode = fn.mode()
        local mode_col = "%#OtherMode#"

        if (mode == "n") then
            mode_col = "%#NormalMode#"
        elseif (mode == "i") then
            mode_col = "%#InsertMode#"
        elseif (mode == "R" or mode == "r" or mode == "Rv") then
            mode_col = "%#ReplaceMode#"
        elseif (mode == "v" or mode == "V") then
            mode_col = "%#VisualMode#"
        elseif (mode == "") then
            mode_col = "%#VisualBlockMode#"
        elseif (mode == "c") then
            mode_col = "%#CommandMode#"
        elseif (mode == "t") then
            mode_col = "%#TerminalMode#"
        end
        cmd("redrawstatus")
        return(mode_col)
    end

    local function mod()
        local modified = api.nvim_buf_get_option(0, "modified")

        if (modified == false) then
            hi("User8", "none", M.colors.basebg, M.colors.basebg)
            return("")
        else
            hi("User8", "none", M.colors.basebg, M.colors.base03)
            return("%7* |%8* *")
        end
    end

    local function filetype()
        local ft = api.nvim_buf_get_option(0, "filetype")

        if (ft == "") then
            return("-")
        else
            return(ft)
        end
    end

    local line = concat({
        colorize_mode(),
        get_mode(),
        "%7* |%2* %t",
        get_branch(),
        "%0*" .. mod(),
        "%0*%=",
        "%6*%l",
        "%7* |%5* " .. filetype(),
        "%0*",
        "%*"
    })

    return(line)
end

opt.statusline = "%!luaeval('Statusline()')"

return(M.colors)

-- vim:ft=lua
