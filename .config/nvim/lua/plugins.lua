local fn = vim.fn
local cmd = vim.cmd

cmd("packadd packer.nvim")
cmd("packadd impatient.nvim")

local impatient, _ = pcall(require, "impatient")

if impatient then
    require("impatient")
end

local present, packer = pcall(require, "packer")

if not present then
    local packer_path = fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"

    fn.delete(packer_path, "rf")
    Bootstrap = fn.system({"git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", packer_path})

    cmd("packadd packer.nvim")

    present, packer = pcall(require, "packer")

    if not present then
        return(nil)
    end
end

local border = ({"╔", "═", "╗", "║", "╝", "═", "╚", "║"})

packer.init({
    depth = 1,
    compile_path = fn.stdpath("config") .. "/lua/packer_compiled.lua",
    display = ({
        title = "packer",
        open_fn = function()
            return require("packer.util").float({border = border})
        end,
        prompt_border = border,
        working_sym = "-",
        error_sym = "x",
        done_sym = ":",
        removed_sym = "#",
        moved_sym = ">",
        header_sym = "═",
    })
})

local use = packer.use

local compiled, _ = pcall(require, "packer_compiled")

if (not compiled) then
    packer.compile()
end
require("packer_compiled")

packer.startup({(function()
    use({
        "wbthomason/packer.nvim",
        event = "BufReadPre"
    })

    use({
        "lewis6991/impatient.nvim",
        event = "BufReadPre"
    })

    use({
        "arzg/vim-sh",
        event = "BufReadPre"
    })

    use({
        "neovimhaskell/haskell-vim",
        ft = ({ "haskell", "lhaskell", "hs", "hls", "hsc" })
    })

    use({
        "purescript-contrib/purescript-vim",
        ft = "purescript"
    })

    use({
        "wlangstroth/vim-racket",
        ft = "racket"
    })

    use({
        "mnacamura/vim-r7rs-syntax",
        ft = ({ "scheme", "r7rs", "r6rs" })
    })

    use({
        "ziglang/zig.vim",
        ft = "zig"
    })

    use({
        "tweekmonster/startuptime.vim",
        cmd = "StartupTime"
    })

    use({
        "norcalli/nvim-colorizer.lua",
        cmd = "ColorizerToggle"
    })

    use({
        "gruvbox-community/gruvbox",
        event = "BufReadPre"
    })

    use({
        "gpanders/nvim-parinfer",
        ft = ({ "clojure", "fennel", "scheme", "racket", "lisp", "haskell" })
    })

    use({
        "neovim/nvim-lspconfig",
        event = "BufReadPre",
        config = [[require("plugins.lspconfig")]],
        requires = ({
            ({
                "hrsh7th/cmp-nvim-lsp",
                after = "nvim-cmp"
            })
        })
    })

    use({
        "nvim-treesitter/nvim-treesitter",
        config = [[require("plugins.treesitter")]],
        ft = ({
            "c",
            "cpp",
            "vim",
            "lua",
            "scala",
            "ocaml",
            "racket",
            "clojure",
            "haskell",
            "typescript",
            "javascript"
        })
    })

    use({
        "windwp/nvim-autopairs",
        event = "InsertEnter",
        config = function ()
            require("nvim-autopairs").setup({
                disable_in_macro = true,
                enable_moveright = true,
                enable_afterquote = true,
                close_triple_quotes = true,
                enable_check_bracket_line = true
            })
        end
    })

    use({
        "numToStr/Comment.nvim",
        tag = "v0.6",
        event = "BufReadPre",
        config = function()
            require("Comment").setup({
                ignore = "^$",
                sticky = true,
                padding = true,
                toggler = ({
                    line = "gcc",
                    block = "gbc"
                }),
                opleader = ({
                    line = "gc",
                    block = "gb"
                }),
                extra = ({
                    above = "gcO",
                    below = "gco",
                    eol = "gcA"
                }),
                mappings = ({
                    basic = true,
                    extra = true,
                    extended = true
                }),
                pre_hook = nil,
                post_hook = nil
            })
        end
    })

    use({
        "hrsh7th/nvim-cmp",
        event = "BufRead",
        config = [[require("plugins.completion")]],
        requires = ({
            ({
                "f3fora/cmp-spell",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-path",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-buffer",
                after = "nvim-cmp"
            }),
            ({
                "L3MON4D3/LuaSnip",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-nvim-lua",
                after = "nvim-cmp"
            }),
            ({
                "saadparwaiz1/cmp_luasnip",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-nvim-lsp",
                after = "nvim-cmp"
            }),
            ({
                "ray-x/cmp-treesitter",
                after = "nvim-cmp"
            }),
            ({
                "lukas-reineke/cmp-rg",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-calc",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-emoji",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-cmdline",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-nvim-lsp-signature-help",
                after = "nvim-cmp"
            })
        })
    })
end)})

if (Bootstrap) then
    packer.sync()
end

-- vim:ft=lua
