local fn = vim.fn
local api = vim.api
local cmp = require("cmp")
local luasnip = require("luasnip")
local cmp_window = require("cmp.utils.window")

function cmp_window:has_scrollbar()
    return(false)
end

local icons = ({
    Text = "[text]",
    Method = "[method]",
    Function = "[fn]",
    Constructor = "[cons]",
    Field = "[field]",
    Variable = "[var]",
    Class = "[class]",
    Interface = "[int]",
    Module = "[mod]",
    Property = "[prop]",
    Unit = "[unit]",
    Value = "[val]",
    Enum = "[enum]",
    Keyword = "[keyword]",
    Snippet = "[snip]",
    Color = "[color]",
    File = "[file]",
    Reference = "[link]",
    Folder = "[folder]",
    EnumMember = "[enum mem]",
    Constant = "[const]",
    Struct = "[struct]",
    Event = "[event]",
    Operator = "[op]",
    TypeParameter = "[tp]"
})

-- local border = ({"┌", "─", "┐", "│", "┘", "─", "└", "│"})
local border = ({"╔", "═", "╗", "║", "╝", "═", "╚", "║"})

cmp.setup({
    snippet = ({
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end
    }),
    experimental = ({
        ghost_text = false
    }),
    view = ({
        entries = "custom"
    }),
    mapping = cmp.mapping.preset.insert({
        ["<C-p>"] = cmp.mapping.select_prev_item(),
        ["<C-n>"] = cmp.mapping.select_next_item(),
        ["<C-d>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.close(),
        ["<CR>"] = cmp.mapping.confirm({
            select = false
        }),
        ["<Tab>"] = function(fallback)
            if (cmp.visible()) then
                cmp.select_next_item()
            elseif (luasnip.expand_or_jumpable()) then
                fn.feedkeys(api.nvim_replace_termcodes("<Plug>luasnip-expand-or-jump", true, true, true), "")
            else
                fallback()
            end
        end,
        ["<S-Tab>"] = function(fallback)
            if (cmp.visible()) then
                cmp.select_prev_item()
            elseif (luasnip.jumpable(-1)) then
                fn.feedkeys(api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true), "")
            else
                fallback()
            end
        end
    }),
    formatting = ({
        fields = ({ "kind", "abbr", "menu" }),
        format = function(_, vim_item)
            vim_item.kind = icons[vim_item.kind]
            return(vim_item)
        end
    }),
    window = ({
        documentation = ({
            border = border,
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None"
        }),
        completion = ({
            border = border,
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None"
        })
    }),
    sources = ({
        ({name = "rg", priority = 4}),
        ({name = "calc", priority = 4}),
        ({name = "path", priority = 1}),
        ({name = "emoji", priority = 3}),
        ({name = "spell", priority = 2}),
        ({name = "luasnip", priority = 3}),
        ({name = "nvim_lua", priority = 1}),
        ({name = "nvim_lsp", priority = 10}),
        ({name = "treesitter", priority = 4}),
        ({name = "nvim_lsp_signature_help", priority = 10}),
        ({name = "buffer", keyword_length = 2, priority = 5})
    }),
    preselect = cmp.PreselectMode.None
})

cmp.setup.cmdline(":", ({
    sources = ({
        ({name = "cmdline"}),
        ({name = "buffer", keyword_length = 1})
    }),
    view = ({
        entries = "custom"
    }),
    window = ({
        documentation = ({
            border = border,
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None"
        }),
        completion = ({
            border = border,
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None"
        })
    })
}))

cmp.setup.cmdline("/", ({
    sources = ({
        ({name = "buffer", keyword_length = 1})
    }),
    view = ({
        entries = "custom"
    }),
    window = ({
        documentation = ({
            border = border,
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None"
        }),
        completion = ({
            border = border,
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None"
        })
    })
}))

-- vim:ft=lua
