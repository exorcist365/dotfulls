local fn = vim.fn
local cmd = vim.cmd
local api = vim.api
local lsp = vim.lsp
local util = lsp.util
local log = require("vim.lsp.log")
local cmp = require("cmp_nvim_lsp")
local lspconfig = require("lspconfig")
local cpb = lsp.protocol.make_client_capabilities()

local capabilities = cmp.update_capabilities(cpb)

local lua_lsp_path = vim.env.XDG_DATA_HOME

local sumneko_root_path = lua_lsp_path .. "/src/lsp/lua-language-server"
local sumneko_binary = sumneko_root_path .. "/bin/lua-language-server"

capabilities.textDocument.completion.completionItem.documentationFormat = ({ "markdown", "plaintext" })
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.preselectSupport = true
capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
capabilities.textDocument.completion.completionItem.labelDetailsSupport = true
capabilities.textDocument.completion.completionItem.deprecatedSupport = true
capabilities.textDocument.completion.completionItem.commitCharactersSupport = true
capabilities.textDocument.completion.completionItem.tagSupport = ({ valueSet = ({ 1 }) })
capabilities.textDocument.completion.completionItem.resolveSupport = ({
    properties = ({
        "documentation",
        "detail",
        "additionalTextEdits"
    })
})
capabilities.textDocument.codeAction = ({
    dynamicRegistration = false,
    codeActionLiteralSupport = ({
        codeActionKind = ({
            valueSet = ({
                "",
                "quickfix",
                "refactor",
                "refactor.extract",
                "refactor.inline",
                "refactor.rewrite",
                "source",
                "source.organizeImports"
            })
        })
    })
})

-- local border = ({
--       ({"┌", "NormalFloat"}),
--       ({"─", "NormalFloat"}),
--       ({"┐", "NormalFloat"}),
--       ({"│", "NormalFloat"}),
--       ({"┘", "NormalFloat"}),
--       ({"─", "NormalFloat"}),
--       ({"└", "NormalFloat"}),
--       ({"│", "NormalFloat"})
-- })

-- local border = ({
--     ({"▛", "NormalFloat"}),
--     ({"▀", "NormalFloat"}),
--     ({"▜", "NormalFloat"}),
--     ({"▐", "NormalFloat"}),
--     ({"▟", "NormalFloat"}),
--     ({"▄", "NormalFloat"}),
--     ({"▙", "NormalFloat"}),
--     ({"▌", "NormalFloat"})
-- })

local border = ({
    ({"╔", "NormalFloat"}),
    ({"═", "NormalFloat"}),
    ({"╗", "NormalFloat"}),
    ({"║", "NormalFloat"}),
    ({"╝", "NormalFloat"}),
    ({"═", "NormalFloat"}),
    ({"╚", "NormalFloat"}),
    ({"║", "NormalFloat"})
})

local config = ({
    virtual_text = ({
        spacing = 2,
        prefix = "*",
        underline = false,
        source = "if_many"
    }),
    float = ({
        source = "if_many",
        border = border,
        scope = "cursor"
    }),
    signs = true,
    underline = false,
    severity_sort = true,
    update_in_insert = false
})

vim.diagnostic.config(config)

local function goto_definition(split_cmd)
    local function handler(_, result, ctx)
        if (result == nil or vim.tbl_isempty(result)) then
            local _ = log.info() and log.info(ctx.method, "No location found")
            return(nil)
        end

        if (split_cmd) then
            cmd(split_cmd)
        end

        if (vim.tbl_islist(result)) then
            util.jump_to_location(result[1])

            if (#result > 1) then
                util.set_qflist(util.locations_to_items(result))
                api.nvim_command("copen")
                api.nvim_command("wincmd p")
            end
        else
            util.jump_to_location(result)
        end
    end
    return(handler)
end

lsp.handlers["textDocument/definition"] = goto_definition("vert split | vert resize -30")
lsp.handlers["textDocument/publishDiagnostics"] = lsp.with(lsp.diagnostic.on_publish_diagnostics, config)

function lspconfig.preview_location(location, context, before_context)
    context = context or 15
    before_context = before_context or 0

    local uri = location.targetUri or location.uri
    if (uri == nil) then
        return(nil)
    end

    local bufnr = vim.uri_to_bufnr(uri)

    if (not api.nvim_buf_is_loaded(bufnr)) then
        fn.bufload(bufnr)
    end

    local range = location.targetRange or location.range
    local contents = api.nvim_buf_get_lines(
        bufnr,
        range.start.line - before_context,
        range["end"].line + 1 + context,
        false
    )

    local filetype = api.nvim_buf_get_option(bufnr, "filetype")

    return util.open_floating_preview(contents, filetype, ({border = border}))
end

local handlers = ({
    ["textDocument/hover"] = lsp.with(lsp.handlers.hover, ({border = border})),
    ["textDocument/signatureHelp"] = lsp.with(lsp.handlers.signature_help, ({border = border}))
})

local orig_util_open_floating_preview = util.open_floating_preview

function util.open_floating_preview(contents, syntax, opts, ...)
    opts = opts or ({})
    opts.border = opts.border or border
    return(orig_util_open_floating_preview(contents, syntax, opts, ...))
end

local function on_attach(_, bufnr)
    local function nnoremap(...) api.nvim_buf_set_keymap(bufnr, "n", ...) end
    local function set_opt(...) api.nvim_buf_set_option(bufnr, ...) end
    local opts = ({ noremap = true, silent = true })

    set_opt("omnifunc", "v:lua.vim.lsp.omnifunc")

    nnoremap("K", "<cmd>lua vim.lsp.buf.hover()<cr>", opts)
    nnoremap("gr", "<cmd>lua vim.lsp.buf.references()<cr>", opts)
    nnoremap("gd", "<cmd>lua vim.lsp.buf.definition()<cr>", opts)
    nnoremap("gD", "<cmd>lua vim.lsp.buf.declaration()<cr>", opts)
    nnoremap("ga", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
    nnoremap("<leader>rn", "<cmd>lua vim.lsp.buf.rename()<cr>", opts)
    nnoremap("gi", "<cmd>lua vim.lsp.buf.implementation()<cr>", opts)
    nnoremap("<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<cr>", opts)
    nnoremap("<C-p>", "<cmd>lua vim.diagnostic.goto_prev()<cr>", opts)
    nnoremap("<C-n>", "<cmd>lua vim.diagnostic.goto_next()<cr>", opts)
    nnoremap("<leader>f", "<cmd>lua vim.lsp.buf.formatting()<cr>", opts)
end

local servers = ({
    "hls",
    "zls",
    "vimls",
    "clangd",
    "metals",
    "ocamllsp",
    "tsserver",
    "clojure_lsp",
    "racket_langserver"
})

for _, server in ipairs(servers) do
    lspconfig[server].setup({
        handlers = handlers,
        on_attach = on_attach,
        capabilities = capabilities,
        flags = ({
            debounce_text_changes = 150
        })
    })
end

lspconfig.rust_analyzer.setup({
    handlers = handlers,
    on_attach = on_attach,
    capabilities = capabilities,
    settings = ({
        ["rust-analyzer"] = ({
            assist = ({
                importPrefix = "by_self",
                importGranularity = "module"
            }),
            cargo = ({
                loadOutDirsFromCheck = true
            }),
            procMacro = ({
                enable = true
            })
        })
    })
})

local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

lspconfig.sumneko_lua.setup({
    handlers = handlers,
    on_attach = on_attach,
    capabilities = capabilities,
    flags = ({
        debounce_text_changes = 150
    }),
    cmd = ({
        sumneko_binary,
        "-E",
        sumneko_root_path .. "/main.lua"
    }),
    settings = ({
        Lua = ({
            runtime = ({
                version = "LuaJIT",
                path = runtime_path
            }),
            diagnostics = ({
                globals = ({ "vim" })
            }),
            workspace = ({
                library = ({
                    library = api.nvim_get_runtime_file("", true)
                })
            }),
            telemetry = ({
                enable = false
            })
        })
    })
})

local signs = ({
    Warn = "~",
    Hint = "-",
    Info = "^",
    Error = ">"
})

for type, icon in pairs(signs) do
    local hl = "DiagnosticSign" .. type
    fn.sign_define(hl, ({ text = icon, texthl = hl, numhl = "" }))
end

-- vim:ft=lua
