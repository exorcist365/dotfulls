local cmd = vim.cmd
local fmt = string.format

cmd([[autocmd BufWritePost dunstrc,nw silent !"${HOME:-"/home/$USER"}/.local/bin/nw"
      autocmd BufWritePost pipewire.conf,pw silent !"${HOME:-"/home/$USER"}/.local/bin/pw"
      autocmd BufWritePost Xresources,xres,xresources silent !xrdb -load %
      autocmd BufWritePost config.h,*.c silent !make "$MAKEFLAGS"
      autocmd BufWritePost config.def.h silent !rm config.h && make "$MAKEFLAGS"
      autocmd BufWritePost *tmux.conf silent !tmux source-file %
      autocmd BufWritePost *.asm silent !make "$MAKEFLAGS"
      autocmd BufWritePost remap silent !%]])

-- cmd("autocmd CursorHold,CursorHoldI * lua vim.lsp.diagnostic.show_line_diagnostics({focusable=false})")

cmd("autocmd TermOpen * startinsert")
-- cmd("autocmd BufWritePre plugins.lua source % | PackerSync")

cmd([[autocmd BufReadPost *
            \ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
            \ |   exe "normal! g`\""
            \ | endif]])

cmd(fmt("autocmd WinEnter * setlocal statusline=%s", "%!luaeval('Statusline()')"))
cmd("autocmd WinLeave * setlocal statusline=''")

cmd([[autocmd BufWritePre * %s/\s\+$//e
      autocmd BufWritePre * %s/\n\+\%$//e
      autocmd BufNewFile,BufRead *.config setlocal syntax=sh
      autocmd BufNewFile,BufRead *.conf setlocal syntax=sh
      autocmd BufNewFile,BufRead *.cfg setlocal syntax=sh
      autocmd BufNewFile,BufRead *.rc setlocal syntax=sh
      autocmd BufNewFile,BufRead *.sh setlocal syntax=sh
      autocmd BufNewFile,BufRead *.patch setlocal syntax=diff
      autocmd BufNewFile,BufRead *.hs,*.hsc setlocal syntax=haskell filetype=haskell
      autocmd BufNewFile,BufRead *.pl setlocal syntax=perl
      autocmd BufNewFile,BufRead *.txt setlocal syntax=text
      autocmd BufNewFile,BufRead *.pad setlocal syntax=md
      autocmd BufNewFile,BufRead *.ms setlocal syntax=groff
      autocmd BufNewFile,BufRead *.asm setlocal ft=fasm]])

cmd([[autocmd Filetype tex setlocal spell spelllang=en_us,bg
      autocmd Filetype html setlocal spell spelllang=en_us,bg
      autocmd Filetype text setlocal spell spelllang=en_us,bg
      autocmd FileType markdown setlocal spell spelllang=en_us,bg]])

-- vim:ft=lua
