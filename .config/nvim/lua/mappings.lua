local fn = vim.fn
local fmt = string.format
local map = vim.api.nvim_set_keymap
local opts = ({ noremap = true, silent = true })

local function nnoremap(k, c)
    map("n", k, c, opts)
end

local function inoremap(k, c)
    map("i", k, c, opts)
end

local function vnoremap(k, c)
    map("v", k, c, opts)
end

local function cnoremap(k, c)
    map("c", k, c, opts)
end

map("n", ";", ":", ({ noremap = true }))

nnoremap("<F1>", "<nop>")
nnoremap("<A-S-j>", "<cmd>resize -5<cr>")
nnoremap("<A-S-k>", "<cmd>resize +5<cr>")
nnoremap("<A-h>", "<cmd>vertical resize -5<cr>")
nnoremap("<A-l>", "<cmd>vertical resize +5<cr>")
nnoremap("<leader>ss", fmt("<cmd>luafile %s<cr>", fn.stdpath("config") .. "/init.lua"))
nnoremap("<leader>so", fmt("<cmd>Explore %s<cr>", fn.stdpath("config") .. "/lua"))
nnoremap("<leader>tt", "<cmd>split | resize -10 | term<cr>")
nnoremap("<leader>ee", "<cmd>Explore .<cr>")
nnoremap("<leader>ff", "<cmd>Explore .<cr>")
nnoremap("<leader>h", "<cmd>wincmd h<cr>")
nnoremap("<leader>j", "<cmd>wincmd j<cr>")
nnoremap("<leader>k", "<cmd>wincmd k<cr>")
nnoremap("<leader>l", "<cmd>wincmd l<cr>")
nnoremap("<leader>v", "<cmd>vertical split<cr>")
nnoremap("<leader>s", "<cmd>split<cr>")
nnoremap("<leader>o", "o<Esc>O")
nnoremap("r", "R")
nnoremap("j", "gj")
nnoremap("k", "gk")
nnoremap("H", "g0")
nnoremap("L", "g$")
nnoremap("Q", "gwip")
nnoremap("U", "<C-r>")

inoremap("<F1>", "<nop>")
inoremap("<C-q>", "<Esc>gwip i")
inoremap("<A-x>", "<C-g>u<Esc>[s1z=`]a<C-g>u")

nnoremap("<leader>pu", "<cmd>PackerSync<cr>")
nnoremap("<leader>pc", "<cmd>PackerClean<cr>")
nnoremap("<leader>pg", "<cmd>PackerUpdate<cr>")

nnoremap("<leader>pt", "<cmd>tabprev<cr>")
nnoremap("<leader>nt", "<cmd>tabnext<cr>")
nnoremap("<leader>tn", "<cmd>tabnew<cr>:Explore .<cr>")

vnoremap("<leader>p", "\"_dP")

nnoremap("<C-q>", "ZZ")
nnoremap("<C-s>", "<cmd>w<cr>")
nnoremap("<C-c>", "<C-w>c<cr>")
nnoremap("<C-w>", "<cmd>set cursorline!<cr>")
nnoremap("<C-x>", "<cmd>set nonumber! norelativenumber!<cr>")

nnoremap("<leader>nn", "<cmd>next<cr>")
nnoremap("<leader>pp", "<cmd>prev<cr>")

nnoremap("n", "nzzzv")
nnoremap("N", "Nzzzv")
nnoremap("J", "mzJ`z")

nnoremap("<S-up>", "<nop>")
nnoremap("<S-down>", "<nop>")
nnoremap("<S-left>", "<nop>")
nnoremap("<S-right>", "<nop>")
nnoremap("<C-up>", "<nop>")
nnoremap("<C-down>", "<nop>")
nnoremap("<C-left>", "<nop>")
nnoremap("<C-right>", "<nop>")
nnoremap("<up>", "<nop>")
nnoremap("<down>", "<nop>")
nnoremap("<left>", "<nop>")
nnoremap("<right>", "<nop>")

nnoremap("<leader>de", "!!date -I<cr>")

inoremap("<S-up>", "<nop>")
inoremap("<S-down>", "<nop>")
inoremap("<S-left>", "<nop>")
inoremap("<S-right>", "<nop>")
inoremap("<C-up>", "<nop>")
inoremap("<C-down>", "<nop>")
inoremap("<C-left>", "<nop>")
inoremap("<C-right>", "<nop>")
inoremap("<up>", "<nop>")
inoremap("<down>", "<nop>")
inoremap("<left>", "<nop>")
inoremap("<right>", "<nop>")

vnoremap("<S-up>", "<nop>")
vnoremap("<S-down>", "<nop>")
vnoremap("<S-left>", "<nop>")
vnoremap("<S-right>", "<nop>")
vnoremap("<C-up>", "<nop>")
vnoremap("<C-down>", "<nop>")
vnoremap("<C-left>", "<nop>")
vnoremap("<C-right>", "<nop>")
vnoremap("<up>", "<nop>")
vnoremap("<down>", "<nop>")
vnoremap("<left>", "<nop>")
vnoremap("<right>", "<nop>")

cnoremap("w!!", "w !ssu tee % >'/dev/null'")

nnoremap ("<A-j>", "<cmd>m .+1<cr>==")
nnoremap ("<A-k>", "<cmd>m .-2<cr>==")
inoremap ("<A-j>", "<Esc><cmd>m .+1<cr>==gi")
inoremap ("<A-k>", "<Esc><cmd>m .-2<cr>==gi")
vnoremap ("<A-j>", "<cmd>m '>+1<cr>gv=gv")
vnoremap ("<A-k>", "<cmd>m '<-2<cr>gv=gv")

-- vim:ft=lua
